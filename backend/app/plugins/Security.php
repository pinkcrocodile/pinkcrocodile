<?php

//Author: Marek Doksanský

use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Acl;

class Security extends Plugin
{

	public function __construct($dependencyInjector)
	{
		$this->_dependencyInjector = $dependencyInjector;
	}

	public function getAcl()
	{
		if (!isset($this->persistent->acl)) {

			$acl = new Phalcon\Acl\Adapter\Memory();

			$acl->setDefaultAction(Phalcon\Acl::DENY);

			//Register roles
			$roles = array(
				'users' => new Phalcon\Acl\Role('Users'),
				'guests' => new Phalcon\Acl\Role('Guests'),
				'admins' => new Phalcon\Acl\Role('Admins')
			);

			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			//Admins area resources
			$adminResources = array();
			foreach ($adminResources as $resource => $actions) {
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
			}

			//Private area resources
			$privateResources = array();
			foreach ($privateResources as $resource => $actions) {
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
			}

			//Public area resources
			$publicResources = array();
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
			}


			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					$acl->allow($role->getName(), $resource, '*');
				}
			}


			foreach ($privateResources as $resource => $actions) {
				foreach ($actions as $action) {
					$acl->allow('Users', $resource, $action);
					$acl->allow('Admins', $resource, $action);
				}
			}


			foreach ($adminResources as $resource => $actions) {
				foreach ($actions as $action) {
					$acl->allow('Admins', $resource, $action);
				}
			}

			$this->persistent->acl = $acl;
		}

		return $this->persistent->acl;
	}

	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{

		$auth = $this->session->get('auth');
		if (!$auth) {
			$role = 'Guests';
		} else if (($auth["role"]) == 1) {
			$role = 'Admins';
		} else {
			$role = 'Users';
		}

		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl();

		$allowed = $acl->isAllowed($role, $controller, $action);
		if ($allowed != Acl::ALLOW) {

			//Display login form

			/*$dispatcher->forward(
					array(
						'controller' => 'session',
						'action' => 'index'
					)
			);*/
			return false;
		}
	}

}
