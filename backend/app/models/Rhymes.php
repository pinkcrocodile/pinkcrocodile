<?php

class Rhymes extends Phalcon\Mvc\Model
{
	public $id;
	public $id_name;
	public $id_rhyme;
	public $id_word;

    public function getSource()
    {

        return 'rhymes';
    }

    static function getById($id)
    {
        $rhyme = Rhymes::findFirst(array(
            "id = :id:",
            "bind" => array('id' => $id)
        ));
        return $rhyme;
    }
}