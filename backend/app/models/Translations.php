<?php


class Translations extends Phalcon\Mvc\Model
{

	public $id;
	public $cs;
	public $en;


	public function getSource()
	{

		return 'translations';
	}

	static function getById($id)
	{
		$translation = Translations::findFirst(array(
			"id = :id:",
			"bind" => array('id' => $id)
		));
		return $translation;
	}

}