<?php

class Words extends Phalcon\Mvc\Model
{
	public $id;
	public $id_name;
	public $img;
	public $pictogram;
	public $video;
	public $sign;
    public $sign_video;
	public $sound;
	public $public;
	public $id_creator;
	public $id_category;

	public function getSource()
	{
		return 'words';
	}

	static function getByCategory($id_category)
	{
		$word = Words::findFirst(array(
			"id_category = :id_category:",
			"bind" => array('id_category' => $id_category)
		));
		return $word;
	}


	static function getById($id)
	{
		$word = Words::findFirst(array(
			"id = :id:",
			"bind" => array('id' => $id)
		));
		return $word;
	}



}