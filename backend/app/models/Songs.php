<?php

/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 9/28/14
 * Time: 9:41 PM
 */
class Songs extends Phalcon\Mvc\Model
{

	public $id;
	public $id_word;
	public $id_name;
	public $id_lyrics;
	public $music;
	public $video;


    public function getSource()
    {

        return 'songs';
    }

    static function getById($id)
    {
        $song = Songs::findFirst(array(
            "id = :id:",
            "bind" => array('id' => $id)
        ));
        return $song;
    }
}