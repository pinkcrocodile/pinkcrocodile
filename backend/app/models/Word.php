<?php


class Word extends Phalcon\Mvc\Model
{

	public $id;
	public $id_name;
	public $img;
	public $pictogram;
	public $video;
	public $sign;
    public $sign_video;
	public $sound;
	public $public;
	public $id_creator;
	public $id_category;

	public function get($id = null)
	{
		$words = $this::find("id=$id");
		foreach ($words as $word) {
			$this->id = $word->id;
			$this->id_name = $word->id_name;
			$this->img = $word->img;
			$this->pictogram = $word->pictogram;
			$this->video = $word->video;
			$this->sign = $word->sign;
            $this->sign_video = $word->sign_video;
			$this->sound = $word->word;
			$this->public = $word->public;
			$this->id_creator = $word->id_creator;
			$this->id_category = $word->id_category;

			return stripslashes($this->toJson());
		}
	}

	public function post($request)
	{
		return $this->save($this->request->getPost(), array(
			'id_name',
			'img',
			'pictogram',
			'video',
			'sign',
            'sign_video',
			'sound',
			'public',
			'id_creator',
			'id_category'
		));
	}

	public function delete($id)
	{
		$word = $this::find("id=$id");
		return $word->delete();
	}

	public function getSource()
	{
		return 'words';
	}

	static function getByCategory($id_category)
	{
		$word = Words::findFirst(array(
			"id_category = :id_category:",
			"bind" => array('id_category' => $id_category)
		));
		return $word;
	}

	public function toJson()
	{
		return json_encode(array(
			'id' => $this->id,
			'id_name' => $this->id_name,
			'img' => $this->img,
			'pictogram' => $this->pictogram,
			'video' => $this->video,
			'sign' => $this->sign,
            'sign_video' => $this->sign_video,
			'sound' => $this->sound,
			'public' => $this->public,
			'id_creator' => $this->id_creator,
			'id_category' => $this->id_category
		));
	}
}