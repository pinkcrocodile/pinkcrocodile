<?php

class Banned extends Phalcon\Mvc\Model
{
	public $id;
	public $id_user;
	public $id_word;

	public function getSource()
	{
		return 'banned';
	}

	static function getById($id)
	{
		$bann = Banned::findFirst(array(
			"id = :id:",
			"bind" => array('$id' => $id)
		));
		return $bann;
	}

	static function getByIdWord($id_word)
	{
		$bann = Banned::findFirst(array(
			"id_word = :id_word:",
			"bind" => array('$id_word' => $id_word)
		));
		return $bann;
	}

	static function getByIdUser($id_user)
	{
		$bann = Banned::findFirst(array(
			"id_user = :id_user:",
			"bind" => array('id_user' => $id_user)
		));
		return $bann;
	}

}