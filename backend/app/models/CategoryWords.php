<?php

/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 8/11/14
 * Time: 10:50 AM
 */
class CategoryWords extends \Phalcon\Mvc\Model
{

	public $id;
	public $id_name;
	public $img;
	public $video;
	public $sign;
	public $sound;
	public $public;
	public $id_creator;
	public $id_category;

	public function get($id)
	{

		$isParent = false;
		$category = Categories::find("id=$id");

		while (!$isParent) {

			$categoryWords = Words::find("id_category=$id");

			foreach ($categoryWords as $categoryWord) {
				$this->id = $categoryWord->id;
				$this->id_name = $categoryWord->id_name;
				$this->img = $categoryWord->img;
				$this->video = $categoryWord->video;
				$this->sign = $categoryWord->sign;
				$this->sound = $categoryWord->sound;
				$this->public = $categoryWord->public;
				$this->id_creator = $categoryWord->id_creator;
				$this->id_category = $categoryWord->id_category;

				return stripslashes($this->toJson());
			}

			if ($category->id_parent == null) {
				$isParent = true;
			} else {
				$temp = $category->id_parent;
				$id = $temp;
				$category = Categories::find("id=$temp");
			}
		}

	}

	public function post($request)
	{
		return $this->save($this->request->getPost(), array('id_name', 'img', 'video', 'sign', 'sound', 'public', 'id_creator', 'id_category'));
	}

	public function delete($id)
	{
		$categoryWord = $this::find("id=$id");
		return $categoryWord->delete();
	}

	public function getSource()
	{
		return 'words';
	}

	static function getByCategory($id_category)
	{
		$categoryWord = Words::findFirst(array(
			"id_category = :id_category:",
			"bind" => array('id_category' => $id_category)
		));
		return $categoryWord;
	}

	public function toJson()
	{
		return json_encode(array('id' => $this->id, 'id_name' => $this->id_name, 'img' => $this->img, 'video' => $this->video, 'sign' => $this->sign,
			'sound' => $this->sound, 'public' => $this->public, 'id_creator' => $this->id_creator, 'id_category' => $this->id_category));
	}

}