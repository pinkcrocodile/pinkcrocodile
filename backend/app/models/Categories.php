<?php

class Categories extends Phalcon\Mvc\Model
{
    public $id;
    public $id_name;
    public $id_parent;
    public $img;

    public function getSource()
    {

        return 'categories';
    }

    static function getById($id)
    {
        $category = Categories::findFirst(array(
            "id = :id:",
            "bind" => array('id' => $id)
        ));
        return $category;
    }
}