<?php

class Users extends Phalcon\Mvc\Model
{
	public $id;
	public $nick;
	public $name;
	public $email;
	public $style;
	public $passwd;
	public $salt;
	public $role;
	public $valid;

	public function getSource()
	{
		return 'users';
	}

	static function getByNick($nick)
	{
		$user = Users::findFirst(array(
			"nick = :nick:",
			"bind" => array('nick' => $nick)
		));
		return $user;
	}

	static function getById($id)
	{
		$user = Users::findFirst(array(
			"id = :id:",
			"bind" => array('id' => $id)
		));
		return $user;
	}

}