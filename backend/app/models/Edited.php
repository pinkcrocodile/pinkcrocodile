<?php

class Edited extends Phalcon\Mvc\Model
{
	public $id;
	public $id_user;
	public $id_word;
	public $video;

	public function getSource()
	{
		return 'edited';
	}

	static function getById($id)
	{
		$edit = Edited::findFirst(array(
			"id = :id:",
			"bind" => array('$id' => $id)
		));
		return $edit;
	}

	static function getByIdWord($id_word)
	{
		$edit = Edited::findFirst(array(
			"id_word = :id_word:",
			"bind" => array('$id_word' => $id_word)
		));
		return $edit;
	}

	static function getByIdUser($id_user)
	{
		$edit = Edited::findFirst(array(
			"id_user = :id_user:",
			"bind" => array('id_user' => $id_user)
		));
		return $edit;
	}

	static function getByVideo($video)
	{
		$edit = Edited::findFirst(array(
			"video = :video:",
			"bind" => array('video' => $video)
		));
		return $edit;
	}

}