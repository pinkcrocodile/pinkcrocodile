<?php

//Author: Marek Doksanský

class UsersController extends Phalcon\Mvc\Controller
{

	public function IndexAction()
	{

	}

	private function _sendResetPasswordMail($email, $password)
	{

		$to = $email;

		//todo this should be in language customizable template
		$subject = 'Resetování hesla na web Pink Crocodile';
		$message = "Dobrý den,\r\n \r\nvaše nové heslo na web školky Pink Crocodile je: " . $password;
		$headers = 'From: pinkCrocodile@webservice.com';

		return mail($to, $subject, $message, $headers);
	}

	public function UserAllowAction()
	{
		$receiveData = $this->request->getJsonRawBody();
		$nick = $receiveData->login;
		$user = Users::getByNick($nick);

		if ($user) {
			$user->valid = 1;
			$this->response->setStatusCode(200, "OK");
		} else {
			$this->response->setStatusCode(403, "Forbidden");
		}
		return $this->response;
	}

	//Created by PMilan edited by MarekD
	public function ResetPasswordAction()
	{
		$this->response->setContentType('application/json'); //vracena data ve formatu json
		$receiveData = $this->request->getJsonRawBody(); //prijimam supr data s FE
		$nick = $receiveData->login;
		$user = Users::getByNick($nick);

		if ($user) {
			$rand_password = Phalcon\Text::random(Phalcon\Text::RANDOM_ALNUM);
			if ($this->_sendResetPasswordMail($user->email, $rand_password)) {
				$user->passwd = crypt($rand_password, $user->salt);
				$user->save();
				$this->response->setStatusCode(200, "OK");
			} else {
				$this->response->setStatusCode(403, "Forbidden");
			}
		} else {
			$this->response->setStatusCode(403, "Forbidden");
		}

		return $this->response;
	}

}
