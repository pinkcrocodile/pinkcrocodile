<?php

//Author: Marek Doksanský

class SessionController extends Phalcon\Mvc\Controller
{

	public function IndexAction()
	{

	}

	public static function _sendRegistrationMail($emailAdress, $username, $password)
	{

		$to = $emailAdress;
		//todo this should be in language customizable template
		$subject = 'Registrace na web Pink Crocodile';
		$message = "Dobrý den,\r\n \r\nprávě jste se zaregistroval/a v aplikaci školky Pink Crocodile. Vaše přihlašovací údaje jsou:\r\npřihlašovací jméno: " . $username . "\r\nheslo: " . $password . " \r\n \r\nPřihlásit se však budete moct až po schválení Vaší registrace naším administrátorem. Vyčkejte, prosím, na email potvrzující ověření Vaší registrace.";
		$headers = 'From: pinkCrocodile@webservice.com';

		return mail($to, $subject, $message, $headers);
	}

	private function _registerSession($user)
	{
		$this->session->set('auth', array(
			'id' => $user->id,
			'nick' => $user->nick,
			'username' => $user->name,
			'role' => $user->role
		));
	}

	public function AuthAction()
	{

		if ($this->request->isPost()) {

			$receiveData = $this->request->getJsonRawBody();

			$nick = $receiveData->login;
			$password = $receiveData->password;

			$user = Users::getByNick($nick);

			$this->response->setContentType('application/json');

			if ($user && $user->valid == 1 && crypt($password, $user->salt) == $user->passwd) {
				$this->_registerSession($user);
				$this->response->setStatusCode(200, "OK");
				$this->response->setContent(json_encode("Prihlaseni probehlo uspesne."));

			} else {
				$this->response->setStatusCode(403, "Forbidden");
				$this->response->setContent(json_encode("Spatne prihlasovaci jmeno nebo heslo."));
			}

		} else {
			//$this->response->setStatusCode(100, "No send data");
			//$this->response->setContent(json_encode("No send data"));
			// todo error code

		}
		return $this->response;
	}


	public function LoggedUserRoleAction()
	{
		$userRole = $this->session->get("auth")["role"];
		$username = $this->session->get("auth")["nick"];

		if ($user = Users::getByNick($username)) {
			$userId = $user->id;
		}

		if ($userRole == "0") {
			$this->response->setStatusCode(200, "OK");
			$responseData = array("role" => "admin", "name" => $username, "id" => $userId);
			$this->response->setContent(json_encode($responseData));
		} else if ($userRole == "1") {
			$this->response->setStatusCode(200, "OK");
			$responseData = array("role" => "user", "name" => $username, "id" => $userId);
			$this->response->setContent(json_encode($responseData));
		} else {
			$this->response->setStatusCode(401, "Unauthorized");
		}

		return $this->response;
	}


	public function LogoutAction()
	{
		$this->session->remove("auth");
		$this->session->destroy();
	}

}
