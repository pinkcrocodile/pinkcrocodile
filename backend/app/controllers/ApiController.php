<?php

/**
 * Created by PhpStorm.
 * User: Tomáš
 * Date: 3.8.14
 * Time: 21:33
 */
class ApiController extends \Phalcon\Mvc\Controller
{

	/** @persistant */
	public $lang = "cs";


	public function indexAction()
	{

	}

	public function categoriesAction($id)
	{
		$category = new Category();

		if ($this->request->isGet()) {
			$ret = $category->get($id);
		}
		if ($this->request->isPost()) {
			$bool = $category->post($this->request);
		}
		if ($this->request->isDelete()) {
			$bool = $category->delete($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();
	}

	public function wordsAction($id)
	{
		$word = new Words();

		if ($this->request->isGet()) {
			$ret = $word->get($id);
		}
		if ($this->request->isPost()) {
			$ret = $word->post($this->request);
		}
		if ($this->request->isDelete()) {
			$ret = $word->delete($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();

	}

	public function rhymesAction($id)
	{
		$rhyme = new Rhyme();

		if ($this->request->isGet()) {
			$ret = $rhyme->get($id);
		}
		if ($this->request->isPost()) {
			$bool = $rhyme->post($this->request);
		}
		if ($this->request->isDelete()) {
			$bool = $rhyme->delete($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();
	}

	public function usersAction($id)
	{
		$user = new User();

		if ($this->request->isGet()) {
			$ret = $user->get($id);
		}
		if ($this->request->isPost()) {
			$bool = $user->post($this->request);
		}
		if ($this->request->isDelete()) {
			$bool = $user->delete($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();
	}

	public function editedAction($id)
	{
		$edit = new Edit();

		if ($this->request->isGet()) {
			$ret = $edit->get($id);
		}
		if ($this->request->isPost()) {
			$bool = $edit->post($this->request);
		}
		if ($this->request->isDelete()) {
			$bool = $edit->delete($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();
	}

	public function categorywordsAction($id)
	{
		$categoryWords = new CategoryWords();

		if ($this->request->isGet()) {
			$ret = $categoryWords->get($id);
		}

		$this->response->setContentType('application/json', 'UTF-8');

		$this->response->setJsonContent($ret);
		$this->response->send();
	}
}