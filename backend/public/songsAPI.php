<?php

/////////////////////////////////////////////////////////////
//-------------------------SONGS---------------------------//
/////////////////////////////////////////////////////////////

/*

  GET /songs/{id}
  POST /songs/{id}
  DELETE /songs/{id}

  id	int(11) Auto Increment
  id_word	int(11)
  id_name	int(11)
  id_lyrics	int(11)
  music	varchar(200)
  video	varchar(200)

 */

//-------------------------------------------------------//
// REST API - GET /songs/{id}
//-------------------------------------------------------//
$app->get('/api/songs/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$phql = "SELECT * FROM Songs S WHERE S.id_word=:id:";
		$song = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		))->getFirst();

		$phql = "SELECT T.cs as id_name FROM Words W JOIN Translations T ON T.id=W.id_name WHERE W.id=:id:";
		$word = $app->modelsManager->executeQuery($phql, array(
			'id' => $song->id_word
		))->getFirst();

		$phql = "SELECT T.cs FROM Translations T WHERE T.id=:id:";
		$songName = $app->modelsManager->executeQuery($phql, array(
			'id' => $song->id_name
		))->getFirst();

		$phql = "SELECT T.cs FROM Translations T WHERE T.id=:id:";
		$songLyrics = $app->modelsManager->executeQuery($phql, array(
			'id' => $song->id_lyrics
		))->getFirst();


		if ($song == false) {
			$response->setStatusCode(409, "Conflict");
		} else {
			$response->setStatusCode(200, "Ok");

			$response->setJsonContent(array(
				'id' => $song->id,
				'word' => $word->id_name,
				'name' => $songName->cs,
				'lyrics' => $songLyrics->cs,
				'music' => $song->music,
				'video' => $song->video
			));
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});
//-------------------------------------------------------//
// REST API SONGS - POST NEW /songs/
//-------------------------------------------------------//

$app->post('/api/songs/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$song = $app->request->getJsonRawBody();
		$phql = "INSERT INTO Songs (id_word, id_name, id_lyrics, music, video) VALUES (:id_name:, :id_name:, :id_lyrics:, :music:, :video:)";

		$status = $app->modelsManager->executeQuery($phql, array(
			'id_word' => $id,
			'id_name' => $song->id_name,
			'id_lyrics' => $song->id_lyrics,
			'music' => $song->music,
			'video' => $song->video
		));

		if ($status->success() == true) {
			$response->setStatusCode(201, "Created");
			$song->id = $status->getModel()->id;
			$response->setJsonContent($song);
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

//-------------------------------------------------------//
// REST API SONGS - POST UPDATE /songs/{id}
//-------------------------------------------------------//

$app->put('/api/songs/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$song = $app->request->getJsonRawBody();

		$phql = "UPDATE Songs SET id_word = :id_word:,id_name = :id_name:,id_lyrics = :id_lyrics:,music = :music:,video = :video: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id,
			'id_word' => $song->id_word,
			'id_name' => $song->id_name,
			'id_lyrics' => $song->id_lyrics,
			'music' => $song->music,
			'video' => $song->video
		));

		if ($status->success() == true) {
			$response->setStatusCode(201, "Created");
			$song->id = $status->getModel()->id;
			$response->setJsonContent($song);
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

//--------------------------------------------------------
// REST API - DELETE /songs/{id}
//--------------------------------------------------------

$app->delete('/api/songs/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();


	//if (getRole($app) != "host") {
	if (true) {
		$phql = "DELETE FROM Songs WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		));

		if ($status->success() == true) {
			$response->setStatusCode(204, "No Content");
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

?>