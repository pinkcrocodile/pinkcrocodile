<?php

/////////////////////////////////////////////////////////////
//-------------------------USERS---------------------------//
/////////////////////////////////////////////////////////////
// REST API - GET /users/{id}
//-------------------------------------------------------
$app->get('/api/users/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	if (getRole($app) == "admin") {
		$phql = "SELECT * FROM Users WHERE id = :id:";
		$user = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		))->getFirst();

		if ($user == false) {
			$response->setStatusCode(409, "Conflict");
		} else {
			$response->setStatusCode(200, "Ok");
			$response->setJsonContent(array(
				'id' => $user->id,
				'nick' => $user->nick,
				'name' => $user->name,
				'email' => $user->email,
				'role' => $user->role,
				'valid' => $user->valid
			));
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

$app->get('/api/users', function () use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	if (getRole($app) == "admin") {
		$phql = "SELECT * FROM Users";
		$users = $app->modelsManager->executeQuery($phql);

		if ($users == false) {
			$response->setStatusCode(409, "Conflict");
		} else {
			$response->setStatusCode(200, "Ok");
			$data = array();
			foreach ($users as $user) {
				$data[] = array(
					'id' => $user->id,
					'nick' => $user->nick,
					'name' => $user->name,
					'email' => $user->email,
					'role' => $user->role,
					'valid' => $user->valid
				);
			}
			$response->setJsonContent($data);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}
	return $response;
});

//--------------------------------------------------------
// REST API - POST /users/{id}
//-------------------------------------------------------

$app->post('/api/users', function () use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	$user = $app->request->getJsonRawBody();
	$salt = $app->security->getSaltBytes();

	//if (empty(Users::getByNick($user->nick))) {
	$phql = "INSERT INTO Users (nick, name, email, passwd, salt, role, valid) VALUES (:nick:, :name:, :email:, :passwd:, :salt:, :role:, :valid:)";
	$status = $app->modelsManager->executeQuery($phql, array(
		'nick' => $user->nick,
		'name' => $user->name,
		'email' => $user->email,
		'passwd' => crypt($user->passwd, $salt),
		'salt' => $salt,
		'role' => $user->role,
		'valid' => 0
	));

	if ($status->success() == true && !empty($salt)) {
		$controller = new SessionController();
		if ($controller::_sendRegistrationMail($user->email, $user->nick, $user->passwd)) {
			$response->setStatusCode(201, "Created");
		} else {
			$response->setStatusCode(208, "Created with errors");
		}
		$user->id = $status->getModel()->id;
		$response->setJsonContent($user);
	} else {
		$response->setStatusCode(409, "Conflict");
		$errors = array();
		foreach ($status->getMessages() as $message) {
			$errors[] = $message->getMessage();
		}

		$response->setJsonContent($errors);
	}
	//} else {
	//    $response->setStatusCode(209, "User nick conflict");
	//}


	return $response;
});

$app->put('/api/users/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) == "admin") {

		$user = $app->request->getJsonRawBody();var_dump("role>".$user->role);var_dump("valid>".$user->valid);

		$phql = "UPDATE Users SET role = :role:, valid = :valid: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id,
			'role' => $user->role,
			'valid' => $user->valid
		));

		if ($status->success() == true) {
			$response->setStatusCode(201, "Created");
			$user->id = $status->getModel()->id;
			$response->setJsonContent($user);
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	/*} else {
		$response->setStatusCode(401, "Unauthorized");
	}*/
	return $response;
});

//--------------------------------------------------------
// REST API - DELETE /users/{id}
//-------------------------------------------------------
$app->delete('/api/users/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();

	if (getRole($app) == "admin") {
		$phql = "DELETE FROM Users WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		));

		if ($status->success() == true) {
			$response->setStatusCode(204, "No content");
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});
