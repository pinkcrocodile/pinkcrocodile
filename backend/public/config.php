<?php
/**
 * @todo For correct function of this app edit this config
 */

$di = new Phalcon\DI\FactoryDefault();

//Set the database service
$di->set('db', function () {
	return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
		"host" => "localhost",
		"username" => "pink",
		"password" => "***",
		"dbname" => "pinkCrocodile",
		'charset' => 'utf8'
	));
});