<?php

/////////////////////////////////////////////////////////////
//-------------------------RHYMES--------------------------//
/////////////////////////////////////////////////////////////

/*

  GET /rhymes/{id}
  POST /rhymes/{id}
  DELETE /rhymes/{id}

  id	int(11) Auto Increment
  id_word	int(11)
  id_name	int(11)
  id_rhyme	int(11)

 */

//-------------------------------------------------------//
// REST API - GET /rhymes/{id}
//-------------------------------------------------------//
$app->get('/api/rhymes/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$phql = "SELECT * FROM Rhymes R WHERE R.id_word=:id:";
		$rhyme = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		))->getFirst();

		$phql = "SELECT T.cs as id_name FROM Words W JOIN Translations T ON T.id=W.id_name WHERE W.id=:id:";
		$word = $app->modelsManager->executeQuery($phql, array(
			'id' => $rhyme->id_word
		))->getFirst();

		$phql = "SELECT T.cs FROM Translations T WHERE T.id=:id:";
		$rhymeName = $app->modelsManager->executeQuery($phql, array(
			'id' => $rhyme->id_name
		))->getFirst();

		$phql = "SELECT T.cs FROM Translations T WHERE T.id=:id:";
		$rhymeRhyme = $app->modelsManager->executeQuery($phql, array(
			'id' => $rhyme->id_rhyme
		))->getFirst();


		if ($rhyme == false) {
			$response->setStatusCode(409, "Conflict");
		} else {
			$response->setStatusCode(200, "Ok");

			$response->setJsonContent(array(
				'id' => $rhyme->id,
				'word' => $word->id_name,
				'name' => $rhymeName->cs,
				'rhyme' => $rhymeRhyme->cs
			));
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});
//--------------------------------------------------------------
$app->get('/api/rhymes', function () use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	if (true) {
		//if (getRole($app) != "host") {
		$phql = "SELECT W.id, T.cs as id_name, W.img, W.video, W.sign, W.sound, W.public,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name";
		$words = $app->modelsManager->executeQuery($phql);

		if ($words == true) {
			$data = array();
			foreach ($words as $word) {
				$data[] = array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'video' => $word->video,
					'sign' => $word->sign,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				);
			}
			$response->setStatusCode(200, "Ok");
			$response->setJsonContent($data);
		} else {
			$response->setStatusCode(409, "Conflict");
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

//-------------------------------------------------------//
// REST API RHYMES - POST NEW /rhymes/
//-------------------------------------------------------//

$app->post('/api/rhymes/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$rhyme = $app->request->getJsonRawBody();
		$phql = "INSERT INTO Rhymes (id_word, id_name, id_rhyme) VALUES (:id_word:, :id_name:, :id_rhyme:)";

		$status = $app->modelsManager->executeQuery($phql, array(
			'id_word' => $id,
			'id_name' => $rhyme->id_name,
			'id_rhyme' => $rhyme->id_rhyme
		));

		if ($status->success() == true) {
			$response->setStatusCode(201, "Created");
			$rhyme->id = $status->getModel()->id;
			$response->setJsonContent($rhyme);
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

//-------------------------------------------------------//
// REST API RHYMES - POST UPDATE /rhymes/{id}
//-------------------------------------------------------//

$app->put('/api/rhymes/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	//if (getRole($app) != "host") {
	if (true) {
		$rhyme = $app->request->getJsonRawBody();

		$phql = "UPDATE Rhymes SET id_word = :id_word:,id_name = :id_name:,id_rhyme = :id_rhyme: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id,
			'id_word' => $rhyme->id_word,
			'id_name' => $rhyme->id_name,
			'id_rhyme' => $rhyme->id_rhyme
		));

		if ($status->success() == true) {
			$response->setStatusCode(201, "Created");
			$rhyme->id = $status->getModel()->id;
			$response->setJsonContent($rhyme);
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

//--------------------------------------------------------
// REST API - DELETE /rhymes/{id}
//--------------------------------------------------------

$app->delete('/api/rhymes/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();


	//if (getRole($app) != "host") {
	if (true) {
		$phql = "DELETE FROM Rhymes WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $id
		));

		if ($status->success() == true) {
			$response->setStatusCode(204, "No Content");
		} else {

			$response->setStatusCode(409, "Conflict");

			$errors = array();
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}

			$response->setJsonContent($errors);
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}

	return $response;
});

