<?php

try {
/////////////////////////////////////////////////////////////
//----------------PHALCON CONFIGURATION--------------------//
/////////////////////////////////////////////////////////////
//Register an autoloader
	$loader = new \Phalcon\Loader();
	$loader->registerDirs(
		array(
			'../app/controllers/',
			'../app/models/',
			'../app/plugins/'
		)
	)->register();

	require_once "config.php";

//Setup a base URI 
	$di->set('url', function () {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/pinkcrocodile/backend/');
		return $url;
	});

	$di->set('security', function () {
		$security = new Phalcon\Security();
//Set the password hashing factor to 12 rounds
		$security->setWorkFactor(12);
		return $security;
	}, true);

	$di->set('dispatcher', function () use ($di) {
		$eventsManager = $di->getShared('eventsManager');
		$security = new Security($di);
		$eventsManager->attach('dispatch', $security);
		$dispatcher = new Phalcon\Mvc\Dispatcher();
		$dispatcher->setEventsManager($eventsManager);
		return $dispatcher;
	});

	$di->setShared('session', function () {
		$session = new Phalcon\Session\Adapter\Files();

		$session->start();
		return $session;
	});

	$app = new Phalcon\Mvc\Micro($di);

	$app->notFound(function () use ($app) {
		$app->response->setStatusCode(404, "Not Found")->sendHeaders();
		echo 'This page was not found!';
	});

	$userController = new UsersController();

	//$app->post('/api/users/debugRegister', array($userController, "DebugRegisterAction"));

	$app->post('/api/users/resetPasswd', array($userController, "ResetPasswordAction"));

	$app->post('/api/users/allow', array($userController, "UserAllowAction"));

	$sessionController = new SessionController();
	$app->post('/api/user/auth', array($sessionController, "AuthAction"));

	$app->get('/api/user/role', array($sessionController, "LoggedUserRoleAction"));

	$app->post('/api/user/logout', array($sessionController, "LogoutAction"));

	$app->get('/api', array(new IndexController(), "IndexAction"));

	require_once 'categoriesAPI.php';
	require_once 'usersAPI.php';
	require_once 'wordsAPI.php';
	require_once 'songsAPI.php';
	require_once 'rhymesAPI.php';


	$app->handle();
} catch (\Phalcon\Exception $e) {

	echo "PhalconException: ", $e->getMessage();
}

function getRole($app)
{
	if ($app->session->has("auth")) {
		switch ($app->session->get("auth")["role"]) {
			case 0:
				return "admin";
				break;
			case 1:
				return "user";
				break;
		}
	} else {
		return "host";
	}
}

function getUserID($app)
{
	return ($app->session->get("auth")["id"]);
}

?>
