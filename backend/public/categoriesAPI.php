<?php

/////////////////////////////////////////////////////////////
//-------------------------CATEGORIES----------------------//
/////////////////////////////////////////////////////////////

/*
  GET /categories{?lang}
  GET /categories/{id}{?lang}
  POST /categories/{id}
  DELETE /categories/{id}

 */

////////////
// REST API - GET /categories/{id}
//-------------------------------------------------------
$app->get('/api/categories/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');


	$phql = "SELECT C.id, C.img, T.cs as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name WHERE C.id=:id:";
	$category = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();


	if ($category == false) {
		$response->setStatusCode(409, "Conflict");
	} else {
		$response->setStatusCode(200, "Ok");

		$response->setJsonContent(array(
			'id' => $category->id,
			'name' => $category->id_name,
			'img' => $category->img
		));
	}
	return $response;
});

$app->get('/api/categories', function () use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	$phql = "SELECT C.id, T.cs as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name";
	$categories = $app->modelsManager->executeQuery($phql);

	if ($categories == false) {
		$response->setStatusCode(409, "Conflict");
	} else {
		$data = array();
		foreach ($categories as $category) {
			$data[] = array(
				'id' => $category->id,
				'name' => $category->id_name
			);
		}
		$response->setStatusCode(200, "Ok");
		$response->setJsonContent($data);
	}

	return $response;
});

$app->get('/api/categories/{lang:[a-z]+}', function ($lang) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');


	$phql = "";
	if ($lang == "cs") {
		$phql = "SELECT C.id, T.cs as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name ";
	}
	if ($lang == "en") {
		$phql = "SELECT C.id, T.en as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name ";
	}
	$category = $app->modelsManager->executeQuery($phql)->getFirst();

//echo $word->id_name;
	if ($category == false) {
		$response->setStatusCode(409, "Conflict");
	} else {
		$response->setStatusCode(200, "Ok");

		$response->setJsonContent(array(
			'id' => $category->id,
			'name' => $category->id_name
		));
	}

	return $response;
});

$app->post('/api/categories', function () use ($app) {
    $response = new Phalcon\Http\Response();
    $response->setContentType('application/json');

    $name = $app->request->getPost('name');
    $files = $app->request->getUploadedFiles();

    //Insert into Translations
    $phql = "INSERT INTO Translations (cs, en) VALUES (:cs:, :en:)";
    $app->modelsManager->executeQuery($phql, array(
        'cs' => $name,
        'en' => $name
    ));

    //Getting id of that translation
    $phql = "SELECT * FROM Translations T WHERE (T.cs LIKE :cs:) OR (T.en LIKE :en:)";
    $translation = $app->modelsManager->executeQuery($phql, array(
        'cs' => $name,
        'en' => $name
    ))->getFirst();
    $translation_id = $translation->id;

    //-------------------------upload img------------------------------
    $format_img = "jpg";
    if ($app->request->isPost()) {
        if ($app->request->hasFiles() == true) {
            try {
                foreach ($files as $file) {
                    switch ($file->getKey()) {
                        case 'img':
                            $target_dir_img = '../../frontend/img/zdroje/';
                            $temp = explode(".", $file->getName());
                            $format_img = end($temp);
                            $target_dir_img = $target_dir_img . $translation_id . '.' . $format_img;
                            move_uploaded_file($file->getTempName(), $target_dir_img);
                            break;
                    }
                }
            } // better be safe than sorry
            catch (Exception $e) {
                var_dump($e);
            }
        }
    }

    //Insert into Category
    $phql = "INSERT INTO Categories (id_name, id_parent, img) VALUES (:id_name:, :id_parent:, :img:)";
    $status = $app->modelsManager->executeQuery($phql, array(
        'id_name' => (int)$translation_id,
        'id_parent' => null,
        'img' => '/img/zdroje/' . $translation_id . '.' . $format_img
    ));

    $response->setStatusCode(201, "Created");
    return $response;
});

$app->delete('/api/categories/{id:[0-9]+}', function ($id) use ($app) {
    $response = new Phalcon\Http\Response();

    $phql = "SELECT * FROM Categories WHERE id = :id:";
    $category = $app->modelsManager->executeQuery($phql, array(
        'id' => $id
    ))->getFirst();

    $phql_delete_category = "DELETE FROM Categories WHERE id = :id:";
    $phql_delete_translation = "DELETE FROM Translations WHERE id = :id:";

    $status_category = $app->modelsManager->executeQuery($phql_delete_category, array(
        'id' => $id
    ));
    $status_translation = $app->modelsManager->executeQuery($phql_delete_translation, array(
        'id' => (int)$category->id_name
    ));


    if ($status_category->success() == true &&
        $status_translation->success() == true
    ) {
        unlink("../../frontend" . $category->img);
        $response->setStatusCode(204, "No content");

    } else {
        $response->setStatusCode(409, "Conflict");
        $errors = array();
        foreach ($status_category->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        foreach ($status_translation->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        $response->setJsonContent($errors);
    }

    return $response;
});

$app->post('/api/categories/edit/{id:[0-9]+}', function ($id) use ($app) {
    $response = new Phalcon\Http\Response();
    $response->setContentType('application/json');

    $name = $app->request->getPost('name');
    $files = $app->request->getUploadedFiles();

    //////////////
    //get category
    //////////////
    $phql = "SELECT * FROM Categories WHERE id = :id:";
    $category = $app->modelsManager->executeQuery($phql, array(
        'id' => $id
    ))->getFirst();

    //UPDATE
    if ($app->request->isPost()) {
        if ($app->request->hasFiles() == true) {
            try {
                foreach ($files as $file) {
                    switch ($file->getKey()) {
                        case 'img':
                            unlink("../../frontend" . $category->img);
                            $target_dir_img = '../../frontend/img/zdroje/';
                            $temp = explode(".", $file->getName());
                            $format_img = end($temp);
                            $target_dir_img = $target_dir_img . $category->id_name . '.' . $format_img;
                            move_uploaded_file($file->getTempName(), $target_dir_img);
                            $img_path = '/img/zdroje/' . $category->id_name . '.' . $format_img;
                            break;
                    }
                }
            } // better be safe than sorry
            catch (Exception $e) {
                var_dump($e);
            }
        }
    }
    //UPDATE NAME
    $phql = "UPDATE Translations SET cs = :cs:, en = :en: WHERE id = :id:";
    $status = $app->modelsManager->executeQuery($phql, array(
        'id' => (int)$category->id_name,
        'cs' => $name,
        'en' => $name
    ));

    //UPDATE FILE
    $phql = "UPDATE Categories SET img = :img: WHERE id = :id:";
    $status = $app->modelsManager->executeQuery($phql, array(
        'id' => $id,
        'img' => $img_path
    ));

    $response->setStatusCode(201, "Created");
    return $response;
});


