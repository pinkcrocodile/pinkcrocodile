<?php

/////////////////////////////////////////////////////////
//-------------------------WORDS-----------------------//
/////////////////////////////////////////////////////////
/*
 *    id	int(11) Auto Increment
 *    id_name	int(11)
 *    img	varchar(50)
 *    video	varchar(200)
 *    sign	varchar(200)
 *    sound	varchar(50)
 *    public	tinyint(1)
 *    id_creator	int(11)
 *    id_category	int(11)
 */

//-------------------------------------------------------
// REST API - GET /categorywords/{id}
//-------------------------------------------------------
$app->get('/api/categorywords/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	if (true) {
		//if (getRole($app) != "host") {
		$tempCategoryID = $id;
		$phql = "SELECT * FROM Categories WHERE id=:id:";
		$categories = $app->modelsManager->executeQuery($phql, array('id' => $tempCategoryID));
//          SORRY ZA ODKOMENT ALE POTREBUJU TY KATEGORIE VIDET
		if ($categories == true) {
			$data = array();
			$data2 = array();
			foreach ($categories as $category) {
				$phql = "SELECT * FROM Categories WHERE id_parent=:id:";
				$subCategories = $app->modelsManager->executeQuery($phql, array('id' => $category->id));
				$data = findCategories($category->id, $app);

				foreach ($subCategories as $subCategory) {
					$data2 = findCategories($subCategory->id, $app);
				}
			}

//                findAllCategories($categories);

			$response->setStatusCode(200, "Ok");
			$response->setJsonContent(array_merge($data, $data2));
		} else {
			$response->setStatusCode(409, "Conflict");
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}
	return $response;
});

//-------------------------------------------------------
//  SEARCH WORDS
//-------------------------------------------------------
$app->get('/api/search/words/{word:[A-z]+}', function ($word) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	if (true) {
		//if (getRole($app) != "host") {
		//ID USER
		if (getRole($app) == "host") {
			$iduser = 0;
		} else {
			$iduser = getUserID($app);
		}
		$phql = "SELECT W.id, T.cs as id_name, W.img, W.pictogram, W.video, W.sign, W.sound, W.public,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE (T.cs LIKE CONCAT(:word:,\"%\") OR T.en LIKE CONCAT(:word:,\"%\")) AND (W.public = 1 OR W.id_creator = $iduser)";
		$words = $app->modelsManager->executeQuery($phql, array('word' => $word));

		if ($words == true) {
			$data = array();
			foreach ($words as $word) {
				$data[] = array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'pictogram' => $word->pictogram,
					'video' => $word->video,
					'sign' => $word->sign,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				);
			}
			$response->setStatusCode(200, "Ok");
			$response->setJsonContent($data);
		} else {
			$response->setStatusCode(409, "Conflict");
		}
	} else {
		$response->setStatusCode(401, "Unauthorized");
	}
	return $response;
});

// REST API - GET /words/{id}
//-------------------------------------------------------
$app->get('/api/words/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	$video = null;
	$display = null;
	//ID USER
	if (getRole($app) == "host") {
		$iduser = 0;
	} else {
		$iduser = getUserID($app);
	}
	if (getRole($app) == "user") {
		//Koukne se jestli ma user editovane video
		$phql = "SELECT * FROM Edited WHERE id_word=:id: AND id_user=$iduser";
		$edited = $app->modelsManager->executeQuery($phql, array('id' => $id));
		//SLEDUJI ZDALI NENI SLOVO BLOKOVANE
		$phql = "SELECT * FROM Banned WHERE id_word=:id: AND id_user=$iduser";
		$banned = $app->modelsManager->executeQuery($phql, array('id' => $id));

		foreach ($edited as $edit) {
			$video = $edit->video;
		}
		foreach ($banned as $bann) {
			$display = $bann->id;
		}
	}

	$phql = "SELECT W.id, T.cs as id_name, W.img, W.pictogram, W.video, W.sign, W.sign_video, W.sound, W.public,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE W.id=:id: AND (W.public = 1 OR W.id_creator = $iduser)";
	$word = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();

	//if(getRole($app) != "host"){

	if ($word == false) {
		$response->setStatusCode(409, "Conflict");
	} else {
		//jestli nejsou zadna videa editovana zobrazi se standartni
		if ($video == null) {
			if ($display == null) {
				$response->setStatusCode(200, "Ok");
				$response->setJsonContent(array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'pictogram' => $word->pictogram,
					'video' => $word->video,
					'sign' => $word->sign,
					'sign_video' => $word->sign_video,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				));
			}
			//jestli jsou editovana zobrazi se editovana misto standartnich videi
		} else {
			//jestli najde id editovaneho slova stejne jako id slova tak nahradi video jinak standart
			if ($display == null) {
				$response->setStatusCode(200, "Ok");
				$response->setJsonContent(array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'pictogram' => $word->pictogram,
					'video' => $video,
					'sign' => $word->sign,
					'sign_video' => $word->sign_video,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				));
			}
		}
	}
	/* } else {
	  $response->setStatusCode(401, "Unauthorized");
	  } */

	return $response;
});


//-------------------------------------------------------
// REST API - POST /words/
//-------------------------------------------------------
$app->post('/api/words', function () use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	$name = $app->request->getPost('name');
	$category = $app->request->getPost('category');
	$rhyme = $app->request->getPost('rhyme');
	$song = $app->request->getPost('song');
	$signRadio = $app->request->getPost('signRadio');
	$files = $app->request->getUploadedFiles();

	//Insert into Translations
	$phql = "INSERT INTO Translations (cs, en) VALUES (:cs:, :en:)";
	$app->modelsManager->executeQuery($phql, array(
		'cs' => $name,
		'en' => $name
	));

	//Getting id of that translation
	$phql = "SELECT * FROM Translations T WHERE (T.cs LIKE :cs:) OR (T.en LIKE :en:)";
	$translation = $app->modelsManager->executeQuery($phql, array(
		'cs' => $name,
		'en' => $name
	))->getFirst();
	$translation_id = $translation->id;

	//-------------------------upload FILES------------------------------
	$format_img = "jpg";
	$format_pictogram = "jpg";
	$format_video = "mp4";
	$format_sound = "mp3";
	$format_songVideo = "mp4";
	$format_sign = "jpg";
	$format_signVideo = "mp4";
	$format_songSound = "mp3";

	if ($app->request->isPost()) {
		if ($app->request->hasFiles() == true) {
			try {
				foreach ($files as $file) {
					switch ($file->getKey()) {
						case 'img':
							$target_dir_img = '../../frontend/img/zdroje/obrazek/';
							$temp = explode(".", $file->getName());
							$format_img = end($temp);
							$target_dir_img = $target_dir_img . $translation_id . '.' . $format_img;
							move_uploaded_file($file->getTempName(), $target_dir_img);
							break;
						case 'pictogram':
							$target_dir_pictogram = '../../frontend/img/zdroje/piktogram/';
							$temp = explode(".", $file->getName());
							$format_pictogram = end($temp);
							$target_dir_pictogram = $target_dir_pictogram . $translation_id . '.' . $format_pictogram;
							move_uploaded_file($file->getTempName(), $target_dir_pictogram);
							break;
						case 'video':
							$target_dir_video = '../../frontend/mov/zdroje/video/';
							$temp = explode(".", $file->getName());
							$format_video = end($temp);
							$target_dir_video = $target_dir_video . $translation_id . '.' . $format_video;
							move_uploaded_file($file->getTempName(), $target_dir_video);
							break;
						case 'sign':
							$target_dir_sign = '../../frontend/img/zdroje/znak/';
							$temp = explode(".", $file->getName());
							$format_sign = end($temp);
							$target_dir_sign = $target_dir_sign . $translation_id . '.' . $format_sign;
							move_uploaded_file($file->getTempName(), $target_dir_sign);
							break;
						case 'sound':
							$target_dir_sound = '../../frontend/sounds/';
							$temp = explode(".", $file->getName());
							$format_sound = end($temp);
							$target_dir_sound = $target_dir_sound . $translation_id . '.' . $format_sound;
							move_uploaded_file($file->getTempName(), $target_dir_sound);
							break;
						case 'songSound':
							$target_dir_songSound = '../../frontend/sounds/pisen/';
							$temp = explode(".", $file->getName());
							$format_songSound = end($temp);
							$target_dir_songSound = $target_dir_songSound . $translation_id . '.' . $format_songSound;
							move_uploaded_file($file->getTempName(), $target_dir_songSound);
							break;
						case 'songVideo':
							$target_dir_songVideo = '../../frontend/mov/zdroje/pisen/';
							$temp = explode(".", $file->getName());
							$format_songVideo = end($temp);
							$target_dir_songVideo = $target_dir_songVideo . $translation_id . '.' . $format_songVideo;
							move_uploaded_file($file->getTempName(), $target_dir_songVideo);
							break;
						case 'signVideo':
							$target_dir_signVideo = '../../frontend/mov/zdroje/znak/';
							$temp = explode(".", $file->getName());
							$format_signVideo = end($temp);
							$target_dir_signVideo = $target_dir_signVideo . $translation_id . '.' . $format_signVideo;
							move_uploaded_file($file->getTempName(), $target_dir_signVideo);
							break;
					}
				}
			} // better be safe than sorry
			catch (Exception $e) {
				var_dump($e);
			}
		}
	}

	if ($signRadio == 0) {
		$signVideo = $app->request->getPost('signUrl');
	} else {
		$signVideo = '/mov/zdroje/znak/' . $translation_id . '.' . $format_signVideo;
	}

	//-------------------------------------------------------------------
	//Insert into Words
	$public_number = 0;
	$user_id = getUserID($app);
	if (getRole($app) == "admin") {
		$public_number = 1;
	}
	$phql = "INSERT INTO Words (id_name, img, pictogram, video, sign, sign_video, sound, public, id_creator, id_category) VALUES (:id_name:, :img:, :pictogram:, :video:, :sign:, :sign_video:, :sound:, :public:, :id_creator:, :id_category:)";
	$status = $app->modelsManager->executeQuery($phql, array(
		'id_name' => (int)$translation_id,
		'img' => '/img/zdroje/obrazek/' . $translation_id . '.' . $format_img,
		'pictogram' => '/img/zdroje/piktogram/' . $translation_id . '.' . $format_pictogram,
		'video' => '/mov/zdroje/video/' . $translation_id . '.' . $format_video,
		'sign' => 'img:/img/zdroje/znak/' . $translation_id . '.' . $format_sign,
		'sign_video' => $signVideo,
		'sound' => '/sounds/' . $translation_id . '.' . $format_sound,
		'public' => (int)$public_number,
		'id_creator' => (int)$user_id,
		'id_category' => (int)$category
	));
	//Getting id of that word
	$phql = "SELECT * FROM Words W WHERE W.id_name = :id_name:";
	$word = $app->modelsManager->executeQuery($phql, array(
		'id_name' => $translation_id
	))->getFirst();
	$word_id = $word->id;


	//Insert into Translations
	$phql = "INSERT INTO Translations (cs, en) VALUES (:cs:, :en:)";
	$app->modelsManager->executeQuery($phql, array(
		'cs' => $rhyme,
		'en' => $rhyme
	));

	//Getting id of that translation
	$phql = "SELECT * FROM Translations T WHERE (T.cs LIKE :cs:) OR (T.en LIKE :en:)";
	$rhyme_sql = $app->modelsManager->executeQuery($phql, array(
		'cs' => $rhyme,
		'en' => $rhyme
	))->getFirst();
	$rhyme_id = $rhyme_sql->id;

	//INSERT INTO RHYMES
	$phql = "INSERT INTO Rhymes (id_word, id_name, id_rhyme) VALUES (:id_word:, :id_name:, :id_rhyme:)";
	$app->modelsManager->executeQuery($phql, array(
		'id_word' => $word_id,
		'id_name' => $translation_id,
		'id_rhyme' => $rhyme_id
	));
	//Insert into Translations
	$phql = "INSERT INTO Translations (cs, en) VALUES (:cs:, :en:)";
	$app->modelsManager->executeQuery($phql, array(
		'cs' => $song,
		'en' => $song
	));

	//Getting id of that translation
	$phql = "SELECT * FROM Translations T WHERE (T.cs LIKE :cs:) OR (T.en LIKE :en:)";
	$song_sql = $app->modelsManager->executeQuery($phql, array(
		'cs' => $song,
		'en' => $song
	))->getFirst();
	$song_id = $song_sql->id;

	//INSERT INTO SONGS
	$phql = "INSERT INTO Songs (id_word, id_name, id_lyrics, music, video) VALUES (:id_word:, :id_name:, :id_lyrics:, :music:, :video:)";
	$app->modelsManager->executeQuery($phql, array(
		'id_word' => $word_id,
		'id_name' => $translation_id,
		'id_lyrics' => $song_id,
		'music' => '/sounds/pisen/' . $translation_id . '.' . $format_songSound,
		'video' => '/mov/zdroje/pisen/' . $translation_id . '.' . $format_songVideo
	));
	$response->setStatusCode(201, "Created");
	return $response;
});

//-------------------------------------------------------
//          Word Edit
//-------------------------------------------------------
$app->post('/api/words/edit/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();
	$response->setContentType('application/json');

	$name = $app->request->getPost('name');
	$rhyme = $app->request->getPost('rhyme');
	$song = $app->request->getPost('song');
	$signRadio = $app->request->getPost('signRadio');
	$files = $app->request->getUploadedFiles();

	//////////////
	//get word
	//////////////
	$phql = "SELECT * FROM Words WHERE id = :id:";
	$word = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();
	//get rhyme
	$phql = "SELECT * FROM Songs WHERE id_word = :id:";
	$song_data = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();
	//get song
	$phql = "SELECT * FROM Rhymes WHERE id_word = :id:";
	$rhyme_data = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();
	//////////////////////////////////////////
	//UPDATE IF ADMIN OR EDITING YOUR OWN WORD
	//////////////////////////////////////////
	if ((getRole($app) == "admin" && $word->public == 1) || (getRole($app) == "user" && $word->public == 0 && getUserID($app) == $word->id_creator)) {

		//UPDATE WORD NAME
		$phql = "UPDATE Translations SET cs = :cs:, en = :en: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $word->id_name,
			'cs' => $name,
			'en' => $name
		));

		//UPDATE SONG
		$phql = "UPDATE Translations SET cs = :cs:, en = :en: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $song_data->id_lyrics,
			'cs' => $song,
			'en' => $song
		));
		//UPDATE RHYME
		$phql = "UPDATE Translations SET cs = :cs:, en = :en: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => $rhyme_data->id_rhyme,
			'cs' => $rhyme,
			'en' => $rhyme
		));

		//UPDATE FILES - at first delete old one and than load new one
		if ($app->request->isPost()) {
			if ($app->request->hasFiles() == true) {
				try {
					foreach ($files as $file) {
						switch ($file->getKey()) {
							case 'img':
								unlink("../../frontend" . $word->img);
								$target_dir_img = '../../frontend/img/zdroje/obrazek/';
								$temp = explode(".", $file->getName());
								$format_img = end($temp);
								$target_dir_img = $target_dir_img . $word->id_name . '.' . $format_img;
								move_uploaded_file($file->getTempName(), $target_dir_img);
								$img_path = '/img/zdroje/obrazek/' . $word->id_name . '.' . $format_img;
								break;
							case 'pictogram':
								unlink("../../frontend" . $word->img);
								$target_dir_pictogram = '../../frontend/img/zdroje/piktogram/';
								$temp = explode(".", $file->getName());
								$format_pictogram = end($temp);
								$target_dir_pictogram = $target_dir_pictogram . $word->id_name . '.' . $format_pictogram;
								move_uploaded_file($file->getTempName(), $target_dir_pictogram);
								$pictogram_path = '/img/zdroje/piktogram/' . $word->id_name . '.' . $format_pictogram;
								break;
							case 'video':
								unlink("../../frontend" . $word->video);
								$target_dir_video = '../../frontend/mov/zdroje/video/';
								$temp = explode(".", $file->getName());
								$format_video = end($temp);
								$target_dir_video = $target_dir_video . $word->id_name . '.' . $format_video;
								move_uploaded_file($file->getTempName(), $target_dir_video);
								$video_path = '/mov/zdroje/video/' . $word->id_name . '.' . $format_video;
								break;
							case 'sign':
								$tempsign = explode("img:", $word->sign);
								unlink("../../frontend" . end($tempsign));
								$target_dir_sign = '../../frontend/img/zdroje/znak/';
								$temp = explode(".", $file->getName());
								$format_sign = end($temp);
								$target_dir_sign = $target_dir_sign . $word->id_name . '.' . $format_sign;
								move_uploaded_file($file->getTempName(), $target_dir_sign);
								$sign_path = 'img:/img/zdroje/znak/' . $word->id_name . '.' . $format_sign;
								break;
							case 'sound':
								unlink("../../frontend" . $word->sound);
								$target_dir_sound = '../../frontend/sounds/';
								$temp = explode(".", $file->getName());
								$format_sound = end($temp);
								$target_dir_sound = $target_dir_sound . $word->id_name . '.' . $format_sound;
								move_uploaded_file($file->getTempName(), $target_dir_sound);
								$sound_path = '/sounds/' . $word->id_name . '.' . $format_sound;
								break;
							case 'songSound':
								unlink("../../frontend" . $song_data->music);
								$target_dir_songSound = '../../frontend/sounds/pisen/';
								$temp = explode(".", $file->getName());
								$format_songSound = end($temp);
								$target_dir_songSound = $target_dir_songSound . $word->id_name . '.' . $format_songSound;
								move_uploaded_file($file->getTempName(), $target_dir_songSound);
								$songSound_path = '/sounds/pisen/' . $word->id_name . '.' . $format_songSound;
								break;
							case 'songVideo':
								unlink("../../frontend" . $song_data->video);
								$target_dir_songVideo = '../../frontend/mov/zdroje/pisen/';
								$temp = explode(".", $file->getName());
								$format_songVideo = end($temp);
								$target_dir_songVideo = $target_dir_songVideo . $word->id_name . '.' . $format_songVideo;
								move_uploaded_file($file->getTempName(), $target_dir_songVideo);
								$songVideo_path = '/mov/zdroje/pisen/' . $word->id_name . '.' . $format_songVideo;
								break;
							case 'signVideo':
								unlink("../../frontend" . $word->sign_video);
								$target_dir_signVideo = '../../frontend/mov/zdroje/znak/';
								$temp = explode(".", $file->getName());
								$format_signVideo = end($temp);
								$target_dir_signVideo = $target_dir_signVideo . $word->id_name . '.' . $format_signVideo;
								move_uploaded_file($file->getTempName(), $target_dir_signVideo);
								$signVideo_path = '/mov/zdroje/znak/' . $word->id_name . '.' . $format_signVideo;
								break;
						}
					}
				} // better be safe than sorry
				catch (Exception $e) {
					var_dump($e);
				}
			}
		}
		//REUPDATE format of files
		$phql = "UPDATE Words SET img = :img: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'img' => $img_path
		));
		$phql = "UPDATE Words SET pictogram = :pictogram: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'pictogram' => $pictogram_path
		));
		$phql = "UPDATE Words SET video = :video: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'video' => $video_path
		));
		$phql = "UPDATE Words SET sign = :sign: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'sign' => $sign_path
		));
		$phql = "UPDATE Words SET sign_video = :sign_video: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'sign_video' => $signVideo_path
		));
		$phql = "UPDATE Words SET  sound = :sound: WHERE id = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'sound' => $sound_path
		));

		$phql = "UPDATE Songs SET music = :music: WHERE id_word = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'music' => $songSound_path
		));
		$phql = "UPDATE Songs SET video = :video: WHERE id_word = :id:";
		$status = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'video' => $songVideo_path
		));

		//if url
		if ($signRadio == 0 && $signVideo_path != null) {
			unlink("../../frontend" . $word->sign_video);
			$signVideo = $app->request->getPost('signUrl');
			$phql = "UPDATE Words SET sign_video = :sign_video: WHERE id = :id:";
			$status = $app->modelsManager->executeQuery($phql, array(
				'id' => (int)$word->id,
				'sign_video' => $signVideo
			));
		}
		//////////////////////////////////////////////
		//ELSE ADD TO EDIT TABLE OR UPDATE EDIT TABLE
		//////////////////////////////////////////////
	} else {
		$user_id = getUserID($app);
		$format_video = "mp4";
		//upload video
		if ($app->request->isPost()) {
			if ($app->request->hasFiles() == true) {
				try {
					foreach ($files as $file) {
						switch ($file->getKey()) {
							case 'video':
								$target_dir_video = '../../frontend/mov/zdroje/video/';
								$temp = explode(".", $file->getName());
								$format_video = end($temp);
								$target_dir_video = $target_dir_video . $word->id_name . '_' . $user_id . '.' . $format_video;
								move_uploaded_file($file->getTempName(), $target_dir_video);
								$video_path = '/mov/zdroje/video/' . $word->id_name . '_' . $user_id . '.' . $format_video;
								break;
						}
					}
				} // better be safe than sorry
				catch (Exception $e) {
					var_dump($e);
				}
			}
		}

		//test if exist
		$phql = "SELECT * FROM Edited WHERE id_word = :id: AND id_user = :id_user:";
		$exist = $app->modelsManager->executeQuery($phql, array(
			'id' => (int)$word->id,
			'id_user' => (int)$user_id
		))->getFirst();
		//
		if ($exist->id == null) {
			$phql = "INSERT INTO Edited (id_user, id_word, video) VALUES (:id_user:, :id_word:, :video:)";
			$status = $app->modelsManager->executeQuery($phql, array(
				'id_user' => (int)$user_id,
				'id_word' => (int)$word->id,
				'video' => '/mov/zdroje/video/' . $word->id_name . '_' . $user_id . '.' . $format_video
			));
		} else {
			unlink("../../frontend" . $exist->video);
			$phql = "UPDATE Edited SET video = :video: WHERE id_word = :id:";
			$status = $app->modelsManager->executeQuery($phql, array(
				'id' => (int)$word->id,
				'video' => $video_path
			));
		}
	}
	$response->setStatusCode(201, "Created");
	return $response;
});

//-------------------------------------------------------
// REST API - DELETE /words/{id}
//-------------------------------------------------------
$app->delete('/api/words/{id:[0-9]+}', function ($id) use ($app) {
	$response = new Phalcon\Http\Response();

	/**
	 * User Verification, getting word data
	 * */
	$id_user = getUserID($app);

	$phql = "SELECT * FROM Words WHERE id = :id:";
	$word = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();

	$phql = "SELECT * FROM Songs WHERE id_word = :id:";
	$song = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();

	$phql = "SELECT * FROM Rhymes WHERE id_word = :id:";
	$rhyme = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	))->getFirst();

	$phql = "SELECT * FROM Edited WHERE id_word = :id:";
	$edits = $app->modelsManager->executeQuery($phql, array(
		'id' => $id
	));

	/**
	 * Admin can delete only public words
	 * */
	if ((getRole($app) == "admin") || ($id_user == $word->id_creator)) {
		$phql_delete_words = "DELETE FROM Words WHERE id = :id:";
		$phql_delete_translations = "DELETE FROM Translations WHERE id = :id_name:";
		$phql_delete_banned = "DELETE FROM Banned WHERE id_word = :id:";
		$phql_delete_edited = "DELETE FROM Edited WHERE id_word = :id:";
		$phql_delete_rhymes = "DELETE FROM Rhymes WHERE id_word = :id:";
		$phql_delete_songs = "DELETE FROM Songs WHERE id_word = :id:";
		$phql_delete_translations_song = "DELETE FROM Translations WHERE id = :id_name:";
		$phql_delete_translations_rhyme = "DELETE FROM Translations WHERE id = :id_name:";

		$status_banned = $app->modelsManager->executeQuery($phql_delete_banned, array(
			'id' => $id
		));
		$status_edited = $app->modelsManager->executeQuery($phql_delete_edited, array(
			'id' => $id
		));
		$status_songs = $app->modelsManager->executeQuery($phql_delete_songs, array(
			'id' => $id
		));
		$status_rhymes = $app->modelsManager->executeQuery($phql_delete_rhymes, array(
			'id' => $id
		));
		$status_words = $app->modelsManager->executeQuery($phql_delete_words, array(
			'id' => $id
		));
		$status_translations_song = $app->modelsManager->executeQuery($phql_delete_translations_song, array(
			'id_name' => (int)$song->id_lyrics
		));
		$status_translations_rhyme = $app->modelsManager->executeQuery($phql_delete_translations_rhyme, array(
			'id_name' => (int)$rhyme->id_rhyme
		));

		$status_translations = $app->modelsManager->executeQuery($phql_delete_translations, array(
			'id_name' => (int)$word->id_name
		));

		if ($status_words->success() == true &&
			$status_rhymes->success() == true &&
			$status_songs->success() == true &&
			$status_translations->success() == true &&
			$status_translations_rhyme->success() == true &&
			$status_translations_song->success() == true
		) {
			unlink("../../frontend" . $word->img);
			unlink("../../frontend" . $word->video);
			$temp = explode("img:", $word->sign);
			unlink("../../frontend" . end($temp));
			unlink("../../frontend" . $word->sound);
			unlink("../../frontend" . $word->sign_video);
			unlink("../../frontend" . $song->music);
			unlink("../../frontend" . $song->video);
			foreach ($edits as $edit) {
				unlink("../../frontend" . $edit->video);
			}

			$response->setStatusCode(204, "No content");
		} else {
			$response->setStatusCode(409, "Conflict");
			$errors = array();
			foreach ($status_words->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			foreach ($status_translations->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			foreach ($status_songs->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			foreach ($status_rhymes->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			foreach ($status_translations_rhyme->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			foreach ($status_translations->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			$response->setJsonContent($errors);
		}
	} /**
	 * If its public word, ban this word for actual user
	 * */
	else {
		$ban_word = "INSERT INTO Banned (id_user, id_word) VALUES (:id_user:, :id_word:)";
		$status_ban_word = $app->modelsManager->executeQuery($ban_word, array(
			'id_user' => $id_user,
			'id_word' => $id
		));
		if ($status_ban_word->success() == true) {
			$response->setStatusCode(204, "No content");
		} else {
			$response->setStatusCode(409, "Conflict");
			$errors = array();
			foreach ($status_ban_word->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			$response->setJsonContent($errors);
		}
	}
	return $response;
});

function findCategories($id, $app)
{
	//get user id
	$iduser = getUserID($app);
	if (getRole($app) == "user") {
		$phql = "SELECT T.cs as id_name, W.id, W.img, W.pictogram, W.video, W.sign, W.sound, W.public,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE (W.id_category=:id: AND W.public = 1) OR (W.id_category=:id: AND W.id_creator = $iduser)";
	} else {
		$phql = "SELECT T.cs as id_name, W.id, W.img, W.pictogram, W.video, W.sign, W.sound, W.public,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE (W.id_category=:id: AND W.public = 1)";
	}
	$words = $app->modelsManager->executeQuery($phql, array('id' => $id));
	$data = array();

	foreach ($words as $word) {
		$idword = (int)$word->id;
		//Koukne se jestli ma user editovane video
		if (getRole($app) == "user") {
			$phql = "SELECT * FROM Edited WHERE id_word=$idword AND id_user=$iduser";
			$edited = $app->modelsManager->executeQuery($phql, array('id' => $id));
			//SLEDUJI ZDALI NENI SLOVO BLOKOVANE
			$phql = "SELECT * FROM Banned WHERE id_word=$idword AND id_user=$iduser";
			$banned = $app->modelsManager->executeQuery($phql, array('id' => $id));
		}
		$video = null;
		$display = null;
		foreach ($edited as $edit) {
			$video = $edit->video;
		}
		foreach ($banned as $bann) {
			$display = $bann->id;
		}
		//jestli nejsou zadna videa editovana zobrazi se standartni
		if ($video == null) {
			if ($display == null) {
				$data[] = array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'pictogram' => $word->pictogram,
					'video' => $word->video,
					'sign' => $word->sign,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				);
			}
			//jestli jsou editovana zobrazi se editovana misto standartnich videi
		} else {
			if ($display == null) {
				$data[] = array(
					'id' => $word->id,
					'name' => $word->id_name,
					'img' => $word->img,
					'pictogram' => $word->pictogram,
					'video' => $video,
					'sign' => $word->sign,
					'sound' => $word->sound,
					'public' => $word->public,
					'id_creator' => $word->id_creator,
					'id_category' => $word->id_category
				);
			}
		}
	}
	return $data;
}

function findAllCategories($result)
{

	$prvniPole = $druhePole = $strom = array();
	$i = 0;

	while ($row = mysql_fetch_array($result)) {

		// priprava pro potomky
		$row['child'] = array();

		// potrebuju uchovat celou promennou kvuli referenci
		$prvniPole[$i] = $row;

		// ulozim referenci / ukazatel ala C(++) / s indexem stranky pro snadny pristup
		$druhePole[$row['page_id']] = & $prvniPole[$i];

		if (empty($row['issub'])) {
			// pridam pres referenci do hlavni vetve stromu
			$strom[] = & $druhePole[$row['page_id']]; // tady si nejsem jisty, jeslti je potreba ampersand, kdyz pracuju s referenci
		} else {
			// doplnim potomka pres referenci
			$druhePole[$row['issub']]['child'][] = & $druhePole[$row['id_page']];
		}
		$i++;
	}
}

?>