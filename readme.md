# app.pink-crocodile.org

[app.pink-crocodile.org](http://app.pink-crocodile.org/) is application helping handicapped preschool children to learn new words with images sounds and videos.
Not only their meaning but also their representation in sign language.
App can be used on tablets od computers and is fully customizable.

# Documentation and help

[Documentation on gDrive](https://docs.google.com/document/d/1qbHfi8s4IvoIz_uXLcVhBya9jrlPrXpsNY-cVls0_zE/edit?usp=sharing)

#Copyright and license

Copyright 2014 FBMI SW team

FBMI SW team members:

| Name            | Email                     |
|-----------------|---------------------------|
| Vít Listík      | tivvitmail@gmail.com      |
| Eva Neuhoferová | neuhoferova.eva@gmail.com |
| Petr Pěnka      | petrpenka@gmail.com       |
| Milan Poláček   | polacek.emilan@gmail.com  |
| Marek Doksanský | doksanskym@gmail.com      |
| Ondřej Klempíř  | ondrej.klempir@seznam.cz  |
| Petr Panoška    | panospet@fbmi.cvut.cz     |
| Vojtěch Havel   | havelvojta@seznam.cz      |
| Michal Reimer   | relag.withdot@centrum.cz  |
| Domink Fiala    | fiala.dom@gmail.com       |
| Tomáš Hrůza     | hruzatom@gmail.com        |
| Václav Roun     | vaclav.roun@gmail.com     |
| Petr Šmíd       | pevita@seznam.cz          |

Project is licensed under GNU General Public License

# How to install
- App requires PHP with [Phalcon Framwork](http://phalconphp.com/en/) installed and MySql
- Mysql example structure is located in DB/pinkCrocodile.sql.gz
- Edit /backend/public/config.php to match your MySql configuration
- Edit /frontend/scripts/config.js to match your API url
- Make dirs frontend/mov,img,sounds writable
- Enjoy!

# How to contribute
Feel free to create pull requests