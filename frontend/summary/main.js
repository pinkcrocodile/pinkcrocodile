/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */

function addTitle(id) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categories/' + id + '?lang=' + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                var category = JSON.parse(this.responseText);
                $(".CategoryTitle").text(category.name);
            }
        }
    };
    xhr.send(null);
}


var app = function (response, numOfCat) {
    console.log(response.responseText);
    addTitle(numOfCat);

    var addWords = function (words) {
        //adds words to document (to WordsList)
        for (var i = 0; i < words.length; i++) {

            var myWord = $("<div>");
            var wordImg = '..' + words[i].pictogram;
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            CatImg.addClass('img-responsive');
            myWord.append(CatImg);
            myWord.addClass("Words");
            myWord.addClass('col-md-3');

            var myLink = "detail/?id=" + words[i].id;
            var myHover = $("<div>");
            myHover.addClass('Hover');
            myWord.hover(function () {
                $(this).append(myHover);
            }, function () {
                $(this).children('.Hover').remove();
            })

            $(".WordsList").append(myWord);
            myWord.wrap('<a href=' + myLink + '></a>');
        }
        ;
    }

    var wordsObj = JSON.parse(response.responseText);
    //console.log(wordsObj);
    addWords(wordsObj);
}

var main = function () {
    document.title = getTranslation('SummaryTitle');
    $('.Home').text(getTranslation('Home').toUpperCase());
    var img = document.createElement("img");
    img.src = "../img/home.png";
    $('.Home').append(img);
    //get number of category from url
    var numOfCat = document.URL.split('=')[1];
    //request to list all words in category
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categorywords/' + numOfCat);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                app(this, numOfCat);
            }
        }
    };
    xhr.send(null);
}


$(document).ready(main);