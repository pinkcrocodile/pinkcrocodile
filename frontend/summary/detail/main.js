/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */

//get number of word from url
var numOfWord = document.URL.split('=')[1];
var categ = 4;
var names = ["", "", ""];

const ParentModeLimit = 10000;
//1=Rhyme, 2=Detail, 3=Song

var changeUrl = function (url) {
    return "../.." + url;
}

function setContentType() {
    switch (categ) {
        case 1:
            ShowRhyme();
            break;
        case 2:
            ShowImage();
            break;
        case 3:
            ShowSign();
            break;
        case 4:
            ShowDetail();
            break;
        case 5:
            ShowVideo();
            break;
        case 6:
            ShowSong();
            break;
    }
}

// MODE handle
function changeModeConfirmed() {
    mode.toggle();
    $('#ModeLink').text(getTranslation(mode.type));
    setContentType();

}

function ModeChangeConfirm(num) {
    if (num == 8) {
        changeModeConfirmed();
    }
}

function changeMode() {
    if (mode.type == 'Parent') {
        changeModeConfirmed()
    }
    else {
        $('.mode-popup').show();
        $('.mode-popup').delay(ParentModeLimit).hide(1);
    }
}
// --------------------

// SHOW & HIDE
var hideEverything = function () {
    $('.TopLine').hide();
    $('.DetailVideo').hide();
    $('.DetailSign').hide();
    $('.DetailSignVideo').hide();
    $('.DetailPicture').hide();
    $('.DetailPictogram').hide();
    $('.SongLyrics').hide();
    $('.SongImage').hide();
    $('.SongVideo').hide();
    $('.RhymeText').hide();

    menuDeactivate();
}

var menuDeactivate = function () {
    $('.TopMenu').each(function (i, v) {
        $(this).removeClass("active")
    });
}

var ShowDetail = function () {
    hideEverything();

    categ = 4;
    $('.Detail').addClass('active');
    $('.DetailPictogram').show();
}

var ShowSong = function () {
    hideEverything();
    categ = 6;

    $('.Song').addClass('active');

    if (mode.type == 'Parent') {
        $('.SongLyrics').show();
    }

    $('.SongImage').show();
    $('.SongVideo').show();
}

var ShowRhyme = function () {
    hideEverything();

    categ = 1;

    $('.Rhyme').addClass('active');

    if (mode.type == 'Parent') {
        $('.RhymeText').show();
    }
}

var ShowImage = function () {
    hideEverything();

    categ = 2;
    $('.Image').addClass('active');
    $('.DetailPicture').show();
}

var ShowVideo = function () {
    hideEverything();

    categ = 5;
    $('.Video').addClass('active');
    $('.DetailVideo').show();
}

var ShowSign = function () {
    hideEverything();

    categ = 3;
    $('.Sign').addClass('active');
    $('.DetailSign').show();
    $('.DetailSignVideo').show();
}
// --------------------

//CREATING
var CreateDetail = function (response) {
    console.log(response.responseText);

    menuDeactivate();

    $('.Detail').addClass('active');

    var wordDetail = function (myWord) {
        names[1] = myWord.name;
        var titleName = $('span');
        titleName.text(names[1].toUpperCase());
        $('.DetailTitle').append(titleName);

        var CatPictogram = $("<img>");
        CatPictogram.attr('src', changeUrl(myWord.pictogram));
        CatPictogram.addClass("img-responsive");

        $('.DetailTitle').append(CatPictogram);

        var CatImg = $("<img>");
        CatImg.attr('src', changeUrl(myWord.img));
        CatImg.addClass("img-responsive");

        $('.DetailPicture').append(CatImg);

        var CatPictogram = $("<img>");
        CatPictogram.attr('src', changeUrl(myWord.pictogram));
        CatPictogram.addClass("img-responsive");

        //sound
        $(".DetailPictogram").addClass('sound');
        $(".DetailPictogram").append(CatPictogram);
        $('.DetailPictogram').append($("<audio id='DetailAudio'/>"));
        $("#DetailAudio").attr('src', changeUrl(myWord.sound));
        addSoundOnClick('DetailAudio');

        //main video
        $(".DetailVideo").append($("<video class='video' id='DetVideo'controls ='controls'/>"));
        $("#DetVideo").attr('src', changeUrl(myWord.video));

        var SignImg = $("<img>");
        $('.DetailSign').append(SignImg);
        SignImg.attr('src', changeUrl(myWord.sign.split(':')[1]));
        SignImg.addClass("img-responsive");

        //sign video
        if (myWord.sign.split(':')[0] == 'video') {
            $(".DetailSign").append($("<video class='video' id='SignVideo' />"));
            $("#SignVideo").attr('src', changeUrl(myWord.sign.split(':')[1]));
        }
        else if (myWord.sign.split(':')[0] == 'yt') {
            $(".DetailSign").append($("<iframe class='video' id='SignVideo'/></iframe>"));
            var path = myWord.sign.split(':')[1] + ":" + myWord.sign.split(':')[2];
            $("#SignVideo").attr('src', path);
        }
        else {
            $(".DetailSignVideo").append($("<video class='video' id='SignVideo' controls ='controls'/>"));
            $("#SignVideo").attr('src', changeUrl(myWord.sign_video));
            //CatImg.addClass("col-md-6");
        }

        addClickToPlayToVid();

        var myHover = $("<div>");
        myHover.addClass('Hover');
        $('.DetailPicture').hover(function () {
            $(this).append(myHover);
        }, function () {
            $(this).children('.Hover').remove();
        });

        $('#DetailLink').text(getTranslation('Sound'));
        $('#RhymeLink').text(getTranslation('Rhyme'));
        $('#SongLink').text(getTranslation('Song'));
        $('#VideoLink').text(getTranslation('Video'));
        $('#SignLink').text(getTranslation('Sign'));
        $('#ImageLink').text(getTranslation('Image'));

        $('#ModeLink').text(getTranslation(mode.type));

        var xhr = new XMLHttpRequest();
        xhr.open('GET', ApiUrl + "/songs/" + numOfWord + "?lang=" + lang);
        xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (typeof cb !== "undefined") {
                    cb(this);
                }
                else {
                    var result = JSON.parse(this.responseText);
                    CreateSong(result);
                }
            }
        };
        xhr.send(null);

        var xhr = new XMLHttpRequest();
        xhr.open('GET', ApiUrl + "/rhymes/" + numOfWord + "?lang=" + lang);
        xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (typeof cb !== "undefined") {
                    cb(this);
                }
                else {
                    var result = JSON.parse(this.responseText);
                    CreateRhyme(result);
                }
            }
        };
        xhr.send(null);
    }

    var word = JSON.parse(response.responseText);

    wordDetail(word);
}

var CreateSong = function (response) {

    $('.SongLyrics').text(response.lyrics);

    //sound
    var CatImg = $("<img>");
    CatImg.attr('src', changeUrl('/img/song.png'));
    CatImg.addClass("img-responsive");

    $('.SongImage').append(CatImg);
    $(".SongImage").append($("<audio id='SongMusic'/>"));
    $("#SongMusic").attr('src', changeUrl(response.music));
    addSoundOnClick2('SongMusic', '.SongImage');

    //video
    $(".SongVideo").append($("<video class='SongVid' id='SongVideo' controls ='controls'/>"));
    $("#SongVideo").attr('src', changeUrl(response.video));
    addClickToPlayToVid2('.SongVid');

}

var CreateRhyme = function (response) {
    $('.RhymeText').text(response.rhyme);
}

// --------------------


var main = function () {
    $('.mode-popup').hide();

    //home button
    document.title = getTranslation('DetailTitle');
    $('.Home').text(getTranslation('Home').toUpperCase());
    var img = document.createElement("img");
    img.src = "../../img/home.png";
    $('.Home').append(img);

    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/words/" + numOfWord + "?lang=" + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                CreateDetail(this);
            }
        }
    };
    xhr.send(null);

    ShowDetail();

//------------------Hammerjs-----------------------------
    var Touchpad = document.getElementById('Touchpad');

    // create a simple instance
    // by default, it only adds horizontal recognizers
    var mc = new Hammer(Touchpad);

    // let the pan gesture support all directions.
    // this will block the vertical scrolling on a touch-device while on the element
    mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });

    // listen to events...
    var left = 0;

    mc.on("panleft", function (ev) {
        left++;
        if (left > 25) {
            if(categ > 0)
                 categ--;

            setContentType();
        }
    });

    var right = 0;
    mc.on("panright", function (ev) {
        right++;
        if (right > 25) {
            if(categ < 6)
                categ++;

            setContentType();
        }
    });
//---------------------------------------------    
}

$(document).ready(main);