function validAlpha(str) {
    var regex = /^[a-zA-ZáäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ]+$/;
    if (!regex.test(str)) {
        return false
    }
    else {
        return true;
    }
}

var autocompleteSearch = function (id) {
    var query = $("#SearchBoxText" + id).val();
    var list = "";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            list = JSON.parse(xhr.responseText);
            console.log(list);
            var hints = new Array();
            for (var i = 0; i < list.length; i++) {
                hints.push(list[i].name);
            }

            $("#SearchBoxText" + id).autocomplete(
                {
                    source: hints
                });
        }
    }
    xhr.open("GET", ApiUrl + "/search/words/" + query + '?lang=' + lang, true);
    xhr.send();
};

var search1 = function (e) {
    e.preventDefault();
    var query = $("#SearchBoxText1").val();
    if (validAlpha(query)) {
        var result = 0;
        result = findWord(query);
        if (result != 0) {
            $("#SearchBox1").remove();
            var myWord = $("<div>");
            var wordImg = '..' + result[2];
//			myWord.css("background-image", "url(" + wordImg + ")");
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            CatImg.addClass("img-responsive");

            myWord.addClass("ImgResult");
            myWord.attr('id', 'ImgSound1');
            $(".Dec1").append(myWord);
            $('.ImgResult').append($("<audio id='DetailAudio1'/>"));
            $("#DetailAudio1").attr('src', '..' + result[4]);
            addSoundOnClick2('DetailAudio1', '#ImgSound1');
            myWord.append(CatImg);

            $('.Dec1').closest(".decisionBox").css("padding", "10px");
        }
        else {
            $("#span1").text("Nenalezeno.").show().fadeOut(2000);
        }
    }
    else {
        $("#span1").text("Zadejte pouze znaky abecedy.").show().fadeOut(2000);
    }
};

var search2 = function (e) {
    e.preventDefault();
    var query = $("#SearchBoxText2").val();
    if (validAlpha(query)) {
        var result = 0;
        result = findWord(query);
        if (result != 0) {
            $("#SearchBox2").remove();
            var myWord = $("<div>");
            var wordImg = '..' + result[2];
//			myWord.css("background-image", "url(" + wordImg + ")");
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            CatImg.addClass("img-responsive");

            myWord.addClass("ImgResult");
            myWord.attr('id', 'ImgSound2');
            $(".Dec2").append(myWord);
            $('.ImgResult').append($("<audio id='DetailAudio2'/>"));
            $("#DetailAudio2").attr('src', '..' + result[4]);
            addSoundOnClick2('DetailAudio2', '#ImgSound2');
            myWord.append(CatImg);

            $('.Dec2').closest(".decisionBox").css("padding", "10px");
        }
        else {
            $("#span2").text("Nenalezeno.").show().fadeOut(2000);
        }
    }
    else {
        $("#span2").text("Zadejte pouze znaky abecedy.").show().fadeOut(2000);
    }
};

var findWord = function (query) {

    //console.log("before:" + query);

    var sdiak = "áäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ";
    var bdiak = "aacdeeillnoooorstuuuuyrzAACDEEILLNOOOORSTUUUUYRZ";

    var cleanText = "";

    for (var charIndex = 0; charIndex < query.length; charIndex++) {
        if (sdiak.indexOf(query.charAt(charIndex)) != -1)
            cleanText += bdiak.charAt(sdiak.indexOf(query.charAt(charIndex)));
        else cleanText += query.charAt(charIndex);
    }

    query = cleanText

    //console.log("after:" + query + " got from: " + cleanText);

    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/search/words/" + query + '?lang=' + lang, false);
    xhr.send(null);

    if (xhr.status == 200) {
        var word = JSON.parse(xhr.responseText);
        var result = new Array(word[0].id, word[0].name, word[0].img, word[0].video, word[0].sound, word[0].categoryId);
        return result;
    }
    else {
        console.log(false)
        return 0;
    }
};


function main() {
    document.title = getTranslation('DecisionTitle');
    $('.Home').text(getTranslation('Home').toUpperCase());
    var img = document.createElement("img");
    img.src = "../img/home.png";
    $('.Home').append(img);

    $('#SearchBoxText1').attr("placeholder", getTranslation('Search'));
    $('#SearchBoxText2').attr("placeholder", getTranslation('Search'));
    autocompleteSearch(1);
    autocompleteSearch(2);
    $('#SearchBoxText1').keyup(function () {
        autocompleteSearch(1)
    });
    $('#SearchBoxText2').keyup(function () {
        autocompleteSearch(2)
    });
    $("#imgBtn1").parents("form").submit(search1);
    $("#imgBtn2").parents("form").submit(search2);
}
$(document).ready(main());
