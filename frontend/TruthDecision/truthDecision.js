function validAlpha(str) {
    var regex = /^[a-zA-ZáäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ]+$/;
    if (!regex.test(str)) {
        return false
    }
    else {
        return true;
    }
}

var autocompleteSearch = function () {
    var query = $("#SearchBoxText1").val();
    var list = "";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            list = JSON.parse(xhr.responseText);
            console.log(list);
            var hints = new Array();
            for (var i = 0; i < list.length; i++) {
                hints.push(list[i].name);
            }
            ;

            $("#SearchBoxText1").autocomplete(
                {
                    source: hints
                });
        }
    }
    xhr.open("GET", ApiUrl + "/search/words/" + query + '?lang=' + lang, true);
    xhr.send();

};

var search = function (e) {
    e.preventDefault();
    var query = $("#SearchBoxText1").val();
    if (validAlpha(query)) {
        var result = 0;
        result = findWord(query);
        if (result != 0) {
            $("#SearchBox1").remove();

            var myWord = $("<div>");
            var wordImg = '..' + result[2];
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            CatImg.addClass("img-responsive");
            myWord.append(CatImg);
//			myWord.css("background-image", "url(" + wordImg + ")");
            myWord.addClass("ImgResult")
            $(".Word").append(myWord);

            var myName = $("<div>");
            myName.addClass("Name");
            myName.append("<p>" + result[1] + "</p>");
            $(".Word").append(myName);

            var imgYes = $("<div>");
            var CatImg = $("<img>");
            CatImg.attr('src', '../img/yes.png');
            imgYes.append(CatImg);
//			imgYes.css("background-image", "url(../img/yes.png)");
            imgYes.addClass("ImgYes");
            imgYes.addClass('img-responsive');
            imgYes.attr('id', 'YesSound');
            $(".Yes").append(imgYes);

            $('.ImgYes').append($("<audio id='DetailAudio1'/>"));
            $("#DetailAudio1").attr('src', "../sounds/yes.mp3");
            addSoundOnClick2('DetailAudio1', '#YesSound');

            var imgNo = $("<div>");
            var CatImg = $("<img>");
            CatImg.attr('src', '../img/no.png');
            imgNo.append(CatImg);
//			imgNo.css("background-image", "url(../img/no.png)");
            imgNo.addClass("ImgNo");
            imgNo.addClass('img-responsive');
            imgNo.attr('id', 'NoSound');
            $(".No").append(imgNo);

            $('.ImgNo').append($("<audio id='DetailAudio2'/>"));
            $("#DetailAudio2").attr('src', "../sounds/no.mp3");
            addSoundOnClick2('DetailAudio2', '#NoSound');

            $('.ImgResult').closest(".truthDecision").css("padding", "10px");
            $('.ImgResult').closest(".truthDecision").css("margin", "none");

        }
        else {
            $("#span1").text("Nenalezeno.").show().fadeOut(2000);
        }
    }
    else {
        $("#span1").text("Zadejte pouze znaky abecedy.").show().fadeOut(2000);
    }
};

var findWord = function (query) {

    var sdiak = "áäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ";
    var bdiak = "aacdeeillnoooorstuuuuyrzAACDEEILLNOOOORSTUUUUYRZ";

    var cleanText = "";

    for (var charIndex = 0; charIndex < query.length; charIndex++) {
        if (sdiak.indexOf(query.charAt(charIndex)) != -1)
            cleanText += bdiak.charAt(sdiak.indexOf(query.charAt(charIndex)));
        else cleanText += query.charAt(charIndex);
    }

    query = cleanText

    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/search/words/" + query + '?lang=' + lang, false);
    xhr.send(null);

    if (xhr.status == 200) {
        var word = JSON.parse(xhr.responseText);
        var result = new Array(word[0].id, word[0].name, word[0].img, word[0].video, word[0].sound, word[0].categoryId);
        return result;
    }
    else {
        console.log(false)
        return 0;
    }
};


function main() {
    document.title = getTranslation('TruthDecisionTitle');
    $('.Home').text(getTranslation('Home').toUpperCase());
    var img = document.createElement("img");
    img.src = "../img/home.png";
    $('.Home').append(img);
    $('#SearchBoxText1').attr("placeholder", getTranslation('Search'));
    $('#SearchBoxText1').keyup(autocompleteSearch);
    $('form').submit(search);
    autocompleteSearch();
}

$(document).ready(main());
