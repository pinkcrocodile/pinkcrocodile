
function disablePopup() {
    $("#toPopup2").fadeOut("normal");
    $("#backgroundPopup2").fadeOut("normal");
}

function reload(){
    window.location.replace('../');
}

function niceAlert(text,redirect){
    $("#popupContent2").empty();
    var message = document.createElement("p");
    message.innerHTML = text;
    $("#popupContent2").append(message);

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "button");
    inputAddButton.setAttribute("value", 'OK');

    $("#popupContent2").append(inputAddButton);

    inputAddButton.onclick = function(){
        redirect===true?reload():disablePopup()};
    $("#toPopup2").fadeIn(500);
    $("#backgroundPopup2").css("opacity", "0.7");
    $("#backgroundPopup2").fadeIn(1);
    $("#backgroundPopup2").click(function () {
        redirect===true?reload():disablePopup()
    });


}

var registrationCheck = function (name, nick, password, email) {

    if (name == "" || nick == "" || password == "" || email == "") {
        niceAlert(getTranslation('FillAll'),false);
    } else {
        registrationFunction(name, nick, password, email);
    }
}

var registrationFunction = function (name, nick, password, email) {
    var role = 0;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", ApiUrl + '/users');
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 201) {
                
                niceAlert(getTranslation('RegistrationSuccessfull') + email + " .",true)

            } else if (this.status == 208) {

                niceAlert(getTranslation('RegistrationSuccessfull2')+ email + getTranslation('EmailNotSent'),true)

            } else {

                niceAlert(getTranslation('RegistrationFailed'),false);
            }
        }
    };

    xhr.send(JSON.stringify({nick: nick, name: name, passwd: password, email: email, role: role}));
}

var main = function () {
    document.title = getTranslation('RegistrationTitle');
    $('.regName').text(getTranslation('Username'));
    $('.nick').text(getTranslation('NickName'));
    $('.Password').text(getTranslation('Password'));
    $('.Register').text(getTranslation('Registration'));
    $('.h1').text(getTranslation('RegistrationTitle'));    
}

$(document).ready(main);