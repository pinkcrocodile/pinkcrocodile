/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */
var ActualSite=1;
var MaxSites=1;
function addImageToCat(id, div) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categories/' + id + '?lang=' + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {

                var category = JSON.parse(this.responseText);
                imgUrlWhole = category.img;
                div.attr('src', '.' + imgUrlWhole);
            }
        }
    };
    xhr.send(null);
}

var app = function (response) {
    var addCategories = function (categories) {
        //adds categories to document (to MainSelection)
              var SiteCounter=1;
             
        for (var i = 0; i < categories.length; i++) {
             if(SiteCounter>4) {
                  SiteCounter=1 ;
                  MaxSites=MaxSites+1;
             }
             
            var myCategory = $("<div>");
            var CatText = $("<div>");
            CatText.addClass('CategoryText');
            CatText.text(categories[i].name.toUpperCase());
            myCategory.append(CatText);

            var CatImg = $("<img>");
            CatImg.addClass("img-responsive");
            myCategory.append(CatImg);

            myCategory.addClass("Category col-md-6");
            var id = categories[i].id;
            addImageToCat(id, CatImg);

            myLink = "summary/?id=" + id;
            myCategory.attr('id', "Cat" + id);
            //myCategory.attr('class', "Site" + b);
            var SitesLink="Sites" ;
            var SiteLink="Site" + MaxSites;
            
            var myHover = $("<div>");
            myHover.addClass('Hover');
            myHover.addClass('Hover');
            myCategory.hover(function () {
                $(this).append(myHover);
            }, function () {
                $(this).children('.Hover').remove();
            })

            $(".MainSelection").append(myCategory);
            
            myCategory.wrap('<a href=' + myLink + ' class="'+SiteLink +' Sites"></a>');
            SiteCounter=SiteCounter+1;
        }
    }

    console.log('response Text:\n' + response.responseText + '\n');
    var categoriesObj = JSON.parse(response.responseText);
    addCategories(categoriesObj);
    HideSites();
}

function generateCategories() {
    //request to list all root categories
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categories?lang=' + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                app(this);
            }
        }
    };
    xhr.send(null);
}

var main = function () {
    $('#CzBut').click(function () {
        console.log('language set to cs');
        document.cookie = 'Lang = cs';
        location.reload();
    });

    $('#EnBut').click(function () {
        console.log('language set to en');
        document.cookie = 'Lang = en';
        location.reload();
    });

    document.title = getTranslation('HomeTitle');
    $('.DecisionLink').text(getTranslation('DecisionLink'));
    $('.TruthDecisionLink').text(getTranslation('TruthDecisionLink'));
    $('#LoginButton').text(getTranslation('LoginButton'));
    $('.Username').text(getTranslation('Username'));
    $('.Password').text(getTranslation('Password'));
    $('.forgot').text(getTranslation('forgot'));
    $('.signIn').text(getTranslation('signIn'));
    $('#LostPasswdButton').text(getTranslation('RestorePass'));
    $('.registration').text(getTranslation('Registration'));
    $('#ResetPasswdButton').text(getTranslation('Send'));

    generateCategories();
  //Sliding between categories    
  $(".ClassRight").click(function(){
  if(MaxSites==1) {}
  else{
  if(ActualSite==MaxSites) {}
  else{
  for (var i = 1; i <= MaxSites; i++) {
  if((ActualSite+1)==i) {}
  else{
  $("."+"Site"+i).hide();
  }
  }
  $("."+"Site"+(ActualSite + 1)).show();
  ActualSite=ActualSite+1;
  }}});
  
  $(".ClassLeft").click(function(){
  if(MaxSites==1) {}
  else{
  if(ActualSite==1) {}
  else{
    for (var i = 1; i <= MaxSites; i++) {
  if((ActualSite-1)==i) {}
  else{
  $("."+"Site"+i).hide();
  }
  }
  $("."+"Site"+(ActualSite - 1)).show();
  ActualSite=ActualSite-1;
  }}});
    
}

var HideSites = function () {
    $("."+"Sites").hide();
    $("."+"Site1").show();
    }
    
$(document).ready(main);



  



