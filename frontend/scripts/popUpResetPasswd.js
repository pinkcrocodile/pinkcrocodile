// JavaScript Document
/*--------------------------
 Author: Petr Pěnka
 Edited by: Marek Doksanský
 Description: show and hide pop-up window with reset password
 ----------------------------
 */

var popUpResetPasswd = function (showhide) {
    if (showhide == "show") {
        document.getElementById('lostPasswd-box').style.visibility = "visible";
        $("#lostPasswd-box").fadeIn(500);
        $('body').append('<div id="mask_2"></div>');
        $('#mask_2').fadeIn(500);


    } else if (showhide == "hide") {
        document.getElementById('lostPasswd-box').style.visibility = "hidden";
        $("#lostPasswd-box").fadeOut(1);
        $("#mask_2").fadeOut(1);
    }
};

//register clicks
$(document).click(function (event) {
    if (!$(event.target).closest('#lostPasswd-box').length & !$(event.target).closest('#LostPasswdButton').length) {
        popUpResetPasswd('hide');
    }
});




