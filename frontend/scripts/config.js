/**
 * @todo For correct function of this app edit this config
 */

var ApiUrl = "http://app.pink-crocodile.org/backend/api";

function getVarFromCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    //if cookie doesn't have searched value set yet:
    if (cname == 'Lang') return "cs";
    else if (cname == 'Mode') return "Parent";
}


var lang = getVarFromCookie('Lang');
function Mode() {
    this.type = getVarFromCookie('Mode');
    this.toggle = function () {
        if (this.type == 'Parent') {
            this.type = 'Child';
            document.cookie = 'Mode = Child';
        }
        else {
            this.type = 'Parent';
            document.cookie = 'Mode = Parent';
        }
    }

};

var mode = new Mode();