// JavaScript Document
/*--------------------------
 Author: Petr Pěnka
 Description: show and hide pop-up window with login
 ----------------------------
 */

var popUp = function (showhide) {
    if (showhide == "show") {
        document.getElementById('login-box').style.visibility = "visible";
        $("#login-box").fadeIn(500);
        $('body').append('<div id="mask"></div>');
        $('#mask').fadeIn(500);


    } else if (showhide == "hide") {
        document.getElementById('login-box').style.visibility = "hidden";
        $("#login-box").fadeOut(1);
        $("#mask").fadeOut(1);
    }
};

//register clicks
$(document).click(function (event) {
    if (!$(event.target).closest('#login-box').length & !$(event.target).closest('#LoginButton').length) {
        popUp('hide');
    }
});




