// JavaScript Document
//Author: Marek Doksanský

function niceAlert2(text,redirect){
    popUpResetPasswd('hide');
    $("#popupContent2").empty();
    var message = document.createElement("p");
    message.innerHTML = text;
    $("#popupContent2").append(message);

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "button");
    inputAddButton.setAttribute("value", 'OK');

    $("#popupContent2").append(inputAddButton);

    inputAddButton.onclick = function(){
        if(redirect)
            reload();
        else{disablePopup(); setTimeout(function(){popUpResetPasswd('show')},0);}};
    $("#toPopup2").fadeIn(500);
    $("#backgroundPopup2").css("opacity", "0.7");
    $("#backgroundPopup2").fadeIn(1);
    $("#backgroundPopup2").click(function () {
        if(redirect)
            reload();
        else{disablePopup(); setTimeout(function(){popUpResetPasswd('show')},0);}

    });
}


var resetPasswdFunction = function (nickname) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", ApiUrl + '/users/resetPasswd');
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                
                niceAlert2(getTranslation('NewPasswordEmail'),false)
            } else {      
               
                niceAlert2(getTranslation('Unable to restore password'),false);
            }
        }
    };
    xhr.send(JSON.stringify({login: nickname}));
}


var go = document.getElementById("lostPasswd-box");



go.addEventListener("keypress", function (e) {
    if (e.keyCode == 13) {
        document.getElementById("ResetPasswdButton").onclick();
    }
});
