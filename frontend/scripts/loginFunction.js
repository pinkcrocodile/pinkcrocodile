// JavaScript Document
/*--------------------------
 Author: Petr Pěnka
 Description: function to control nickname and password
 of user for login
 ----------------------------
 */
function disablePopup() {
    $("#toPopup2").fadeOut("normal");
    $("#backgroundPopup2").fadeOut("normal");
}

function niceAlert(text,redirect){
    popUp('hide');
    $("#popupContent2").empty();
    var message = document.createElement("p");
    message.innerHTML = text;
    $("#popupContent2").append(message);

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "button");
    inputAddButton.setAttribute("value", 'OK');

    $("#popupContent2").append(inputAddButton);

    inputAddButton.onclick = function(){
        if(redirect)
            reload();
        else{disablePopup(); setTimeout(function(){popUp('show')},0);}};
    $("#toPopup2").fadeIn(500);
    $("#backgroundPopup2").css("opacity", "0.7");
    $("#backgroundPopup2").fadeIn(1);
    $("#backgroundPopup2").click(function () {
        if(redirect)
            reload();
        else{disablePopup(); setTimeout(function(){popUp('show')},0);}

    });
}



var loginFunction = function (nickname, password) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", ApiUrl + '/user/auth');
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                window.location.replace('administration');
            } else {
                niceAlert(JSON.parse(this.responseText),false);
            }
        }
    };
    xhr.send(JSON.stringify({login: nickname, password: password}));
}

var go = document.getElementById("login-box");
var signIn = document.getElementById("SignInButton");

go.addEventListener("keypress", function (e) {
    if (e.keyCode == 13) {
        document.getElementById("SignInButton").onclick();
    }
});
