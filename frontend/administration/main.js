var categoryNumber = "";
var wordNumber = "";

var makeAccordColaps = function (DivClassOrId) {
    $(DivClassOrId).accordion({
        collapsible: true
    });
};

function setWordNumber(value) {
    wordNumber = value;
}

function setCategoryNumber(value) {
    categoryNumber = value;
}

var listWordsInCategory = function (numOfCat) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categorywords/' + numOfCat+'?lang=' + lang, false);
    xhr.send(null);
    var words = JSON.parse(xhr.responseText);

    return words;
};


var fillCategories = function (categories, role, userId) {
//no cats, only categories :D


    //for cycle through categories
    for (var i = 0; i < categories.length; i++) {
        var title = $("<h3>");
        title.addClass("catWords&" + categories[i].id);
        title.text(categories[i].name);
        $(".CategoriesList").append(title);
        var content = $("<div>");
        content.addClass("AdministrationWordsList");
        content.addClass("catWords&" + categories[i].id);
        var words = listWordsInCategory(categories[i].id)

        //for cycle adding all words in category
        for (var j = 0; j < words.length; j++) {


            var myWord = $("<div>");
            var wordImg = '..' + words[j].pictogram;
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            myWord.append(CatImg);
            myWord.css("position", "relative");
            myWord.addClass("AdministrationWords");
            var options = $("<div>");
            options.addClass('AdministrationOptions');
            var Edit = $("<div>");


            //Edit.attr("id", "edit"+"&"+words[j].id+"&"+categories[i].id);
            var OptionDivider = $("<div>");
            OptionDivider.addClass('OptionDivider');
            options.append(OptionDivider);

            Edit.addClass('Edit');

            var Remove = $("<div>");
            Remove.addClass('Remove');
            options.append(Remove);

            var RemoveImgDiv = $("<div>");
            RemoveImgDiv.addClass('RemoveImg');
            Remove.append(RemoveImgDiv);

            var RemoveImg = $("<img>");
            RemoveImgDiv.append(RemoveImg);
            RemoveImg.attr('src', '../img/delete.png');
            Remove.click(function () {
                removeWordPopup(this.parentNode.parentNode.id, setWordNumber);
            });

            var EditImgDiv = $("<div>");
            EditImgDiv.addClass("EditImg");
            Edit.append(EditImgDiv);
            options.append(Edit);
            var EditImg = $("<img>");
            EditImgDiv.append(EditImg);
            myWord.append(options);
            Edit.click(function () {
                editWordPopup(this.parentNode.parentNode.id, setWordNumber);
            });
            EditImg.attr('src', '../img/pencil.png');
            myWord.attr("id", "word" + "&" + words[j].id);
            $(content).append(myWord);


        }

        $(".CategoriesList").append(content);


        //button for adding new word
        var addButton = $("<div>");
        var ButImg = $("<img>");
        ButImg.attr('src', '../img/addButton.png');
        addButton.append(ButImg);
        addButton.addClass("AdministrationWords");
        addButton.addClass("AddButton");
        addButton.attr("id", "addBtn" + "&" + categories[i].id);
        $(content).append(addButton);
        addButton.click(function () {
            addNewWordPopup(this.id, setCategoryNumber);
        });
    }
    ;

    makeAccordColaps('.CategoriesList');

};

function generateUserManagment(users) {
    var content = $("<div>");
    content.addClass("AdministrationWordsList");

    for (var i = 0; i < users.length; i++) {
        var myWord = $("<div>");

        var wordImg = '../img/addButton.png';

        //   users[i].valid==1?myWord.addClass("Purple"):myWord.addClass("LightPurple");

        myWord.addClass("Purple");

        myWord.css("position", "relative");
        myWord.addClass("Users");

        var title = $("<div>");
        title.addClass('UserTitle');
        users[i].name = (!users[i].name) ? '' : users[i].name;
        var name = users[i].name.length < 10 ? users[i].name : users[i].name.substring(0, 8) + '...'; //Proudly written by 'Vojta'   ¯\_(ツ)_/¯
        title.text(name);
        myWord.append(title);

        var options = $("<div>");
        options.addClass('UserOptions');

        /*        var OptionDivider = $("<div>");
         OptionDivider.addClass('UsersOptionDivider');
         options.append(OptionDivider);*/


        /* var Remove = $("<div>");
         Remove.addClass('UserRemove');
         options.append(Remove);
         var RemoveImgDiv = $("<div>");
         RemoveImgDiv.addClass('UserRemoveImg');
         Remove.append(RemoveImgDiv);
         var RemoveImg = $("<img>");
         RemoveImgDiv.append(RemoveImg);
         RemoveImg.attr('src', '../img/delete.png');
         Remove.click(function () {
         PopupUserRemove(this.parentNode.parentNode.id);
         });
         */
        var Edit = $("<div>");
        Edit.addClass('UserEdit');
        var EditImgDiv = $("<div>");
        EditImgDiv.addClass("UserEditImg");
        Edit.append(EditImgDiv);
        options.append(Edit);
        var EditImg = $("<img>");
        EditImgDiv.append(EditImg);
        myWord.append(options);
        Edit.click(function () {
            UserEdit(this.parentNode.parentNode.id);
        });
        EditImg.attr('src', '../img/pencil.png');
        myWord.attr("id", "user" + "&" + users[i].id);
        $(content).append(myWord);

    }
    $(".UserManagment").append(content);


}

function generateCatagoriesManagment(categories) {
    var content = $("<div>");
    content.addClass("AdministrationWordsList");

    for (var i = 0; i < categories.length; i++) {

        function MakeCategory(id, img) {

            var myWord = $("<div>");

            var wordImg = '../img/addButton.png';


            var CatImg = $("<img>");
            CatImg.attr('src', img);
            CatImg.addClass('AdmCatImg');
            myWord.append(CatImg);
            myWord.addClass("Purple");


            myWord.css("position", "relative");
            myWord.addClass("AdministrationWords");

            var options = $("<div>");
            options.addClass('AdministrationOptions');

            var OptionDivider = $("<div>");
            OptionDivider.addClass('OptionDivider');
            options.append(OptionDivider);


            var Remove = $("<div>");
            Remove.addClass('Remove');
            options.append(Remove);
            var RemoveImgDiv = $("<div>");
            RemoveImgDiv.addClass('RemoveImg');
            Remove.append(RemoveImgDiv);
            var RemoveImg = $("<img>");
            RemoveImgDiv.append(RemoveImg);
            RemoveImg.attr('src', '../img/delete.png');
            Remove.click(function () {
                PopupCatRemove(this.parentNode.parentNode.id);
            });

            var Edit = $("<div>");
            Edit.addClass('Edit');
            var EditImgDiv = $("<div>");
            EditImgDiv.addClass("EditImg");
            Edit.append(EditImgDiv);
            options.append(Edit);
            var EditImg = $("<img>");
            EditImgDiv.append(EditImg);
            myWord.append(options);
            myWord.attr("id", "cat" + "&" + id);
            $(content).append(myWord);
            Edit.click(function () {
                addNewCat('edit', this.parentNode.parentNode.id);
            });
            EditImg.attr('src', '../img/pencil.png');


        }


        var xhr = new XMLHttpRequest();
        xhr.open('GET', ApiUrl + '/categories/' + categories[i].id);
        xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (typeof cb !== "undefined") {
                    cb(this);
                }
                else {

                    var Cat = JSON.parse(this.responseText);
                    MakeCategory(Cat.id, '..' + Cat.img);
                }
            }
        };
        xhr.send(null);

    }
    //button for adding new category
    var addButton = $("<div>");
    var ButImg = $("<img>");
    ButImg.attr('src', '../img/addButton.png');
    addButton.append(ButImg);
    addButton.addClass("AdministrationWords");
    addButton.addClass("AddButton");
    addButton.attr("id", "addBtnNewCat");
    $(content).append(addButton);
    addButton.click(function () {
        addNewCat('add', null);
    });


    $(".CategoriesManagment").append(content);


}

function askForUsers() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/users/');
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {

                var users = JSON.parse(this.responseText);
                generateUserManagment(users);
            }
        }
    };
    xhr.send(null);

}


var main = function (role, userId) {
    document.title = getTranslation('AdministrationTitle');
    $('.Home1').text(getTranslation('Home').toUpperCase());
    var img = document.createElement("img");
    img.src = "../img/home.png";
    $('.Home1').append(img);

    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/categories?lang=' + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                var categories = JSON.parse(this.responseText);
                if (role == 'admin') {

                    var title = $("<h3>");
                    title.text(getTranslation('Words'));
                    $(".MainSelectionList").append(title);
                    var content = $("<div>");
                    content.addClass("CategoriesList");
                    $(".MainSelectionList").append(content);

                    var title = $("<h3>");
                    title.text(getTranslation('Categories'));
                    $(".MainSelectionList").append(title);
                    var content = $("<div>");
                    content.addClass("CategoriesManagment");
                    $(".MainSelectionList").append(content);

                    var title = $("<h3>");
                    title.text(getTranslation('Users'));
                    $(".MainSelectionList").append(title);
                    var content = $("<div>");
                    content.addClass("UserManagment");
                    $(".MainSelectionList").append(content);

                    $(".MainSelectionList").append(content);
                    $(".MainSelectionList").accordion({
                        collapsible: true,
                        heightStyle: "content"
                    });

                    //getter
                    var autoHeight = $('.MainSelectionList').accordion("option", "autoHeight");
//setter
                    $('.MainSelectionList').accordion("option", "autoHeight", false);

                    fillCategories(categories, role, userId);
                    askForUsers();
                    generateCatagoriesManagment(categories);
                }
                else {
                    $(".MainSelectionList").addClass('CategoriesList')
                    fillCategories(categories, role, userId);
                }
            }
        }
    };
    xhr.send(null);


};

var loginCheck = function () {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", ApiUrl + '/user/role');
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onreadystatechange = function () {

        if (this.readyState == 4) {
            if (this.status == 200) {
                var user = JSON.parse(this.responseText);

                var userMenu = $("<div>");
                userMenu.addClass("userMenu");
                $(".loggedUser").append(userMenu);

                var thisUser = $("<div>");
                thisUser.addClass("thisUser");
                thisUser.attr('onclick', "logOut('show')");
                userMenu.append(thisUser);
                $(".thisUser").text(user.role + ": " + user.name);

                var logout = $("<div>");
                logout.addClass("logout");
                logout.attr('onclick', "logOff()");
                userMenu.append(logout);
                $(".logout").text("Odhlásit");

                //hidden user id and user role
                var inputUserId = document.createElement("input");
                inputUserId.setAttribute("type", "text");
                inputUserId.setAttribute("id", "userid");
                inputUserId.setAttribute("value", user.id);
                inputUserId.style.display = "none";
                userMenu.append(inputUserId);
                var inputUserRole = document.createElement("input");
                inputUserRole.setAttribute("type", "text");
                inputUserRole.setAttribute("id", "userrole");
                inputUserRole.setAttribute("value", user.role);
                inputUserRole.style.display = "none";
                userMenu.append(inputUserRole);

                main(user.role, user.id);

            } else {
                window.location.replace('../');
            }
        }
    };
    xhr.send();
}

var logOut = function (showhide) {
    if (showhide == "show") {
        $(".logout").css("visibility", "visible");
        logOut.fadeIn(500);


    } else if (showhide == "hide" && $(".logout").css("visibility") == "visible") {
        $(".logout").css("visibility", "hidden");
        ;
        logOut.fadeOut(1);
    }
}

//register clicks
$(document).click(function (event) {
    if (!$(event.target).closest('.userMenu').length & !$(event.target).closest('.thisUser').length) {
        logOut('hide');
    }
});

var logOff = function () {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", ApiUrl + '/user/logout');
    xhr.send();
    window.location.reload();
}

loginCheck();
