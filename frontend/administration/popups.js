/**
 * Created by Vojta on 28. 10. 2014.
 */
var categoryNumber = "";
var wordNumber = "";

function reload(){
  window.location.reload(true);
}

function niceAlert(text){
    $("#popupContent2").empty();
    var message = document.createElement("p");
    message.innerHTML = text;
    $("#popupContent2").append(message);

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "button");
    inputAddButton.setAttribute("value", 'OK');

    $("#popupContent2").append(inputAddButton);

    inputAddButton.onclick = function(){reload()};
    $("#toPopup2").fadeIn(500);
    $("#backgroundPopup2").css("opacity", "0.7");
    $("#backgroundPopup2").fadeIn(1);
    $("#backgroundPopup2").click(function () {
        reload();
    });


}

var addNewWordPopup = function (categoryId, set) {
    categoryNumber = categoryId.split('&')[1];
    set(categoryNumber);
    loadPopup();
    initNewWordPopup();
};

var editWordPopup = function (id, set) {
    //console.log(id);
    wordNumber = id.split('&')[1];
    set(wordNumber);
    loadEditWordPopup();
    initEditWordPopup();
};

var removeWordPopup = function (id, set) {
    //console.log(id);
    wordNumber = id.split('&')[1];
    set(wordNumber);
    initRemoveWordPopup();
};

function removeWord() {

    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', ApiUrl + '/words/' + wordNumber);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                //console.log('Resptext:' + this.responseText);
                if (this.status == 204) {

                    setTimeout(function(){disablePopup();niceAlert(getTranslation('Success'))},0);

                }
                else {
                    setTimeout(function(){disablePopup();niceAlert(getTranslation('Fail'))},0);
                }
            }
        }
    };
    xhr.send(null);
}
function loadPopup() {
    $("#popupContent").empty();
    //creating hidden iframe
    var iframe = document.createElement("iframe");
    iframe.setAttribute('name', "hiddenFrame");
    iframe.setAttribute('id', "hiddenFrame");
    iframe.setAttribute('class', "iframe");
    iframe.style.display = "none";
    //creating form
    var form = document.createElement("form");
    form.setAttribute('action', ApiUrl + '/words'+'?lang='+lang);
    form.setAttribute('method', "post");
    form.setAttribute('enctype', "multipart/form-data");
    form.setAttribute('target', "hiddenFrame");
    //creating inputs
    var inputCategory = document.createElement("input");
    inputCategory.setAttribute("type", "text");
    inputCategory.setAttribute("name", "category");
    inputCategory.setAttribute("value", categoryNumber);
    inputCategory.style.display = "none";
    var inputName = document.createElement("input");
    inputName.setAttribute("type", "text");
    inputName.setAttribute("name", "name");
    var inputImg = document.createElement("input");
    inputImg.setAttribute("type", "file");
    inputImg.setAttribute("name", "img");
    var inputPictogram = document.createElement("input");
    inputPictogram.setAttribute("type", "file");
    inputPictogram.setAttribute("name", "pictogram");
    var inputVideo = document.createElement("input");
    inputVideo.setAttribute("type", "file");
    inputVideo.setAttribute("name", "video");
    var inputSound = document.createElement("input");
    inputSound.setAttribute("type", "file");
    inputSound.setAttribute("name", "sound");
    var inputSignImg = document.createElement("input");
    inputSignImg.setAttribute("type", "file");
    inputSignImg.setAttribute("name", "sign");

    var inputRadioVideo = document.createElement("input");
    inputRadioVideo.setAttribute("type", "radio");
    inputRadioVideo.setAttribute("name", "signRadio");
    inputRadioVideo.setAttribute("value", "1");
    inputRadioVideo.setAttribute("checked", "checked");
    var inputRadioLink = document.createElement("input");
    inputRadioLink.setAttribute("type", "radio");
    inputRadioLink.setAttribute("name", "signRadio");
    inputRadioLink.setAttribute("value", "0");

    var inputSignVideo = document.createElement("input");
    inputSignVideo.setAttribute("type", "file");
    inputSignVideo.setAttribute("name", "signVideo");
    var inputSignLink = document.createElement("input");
    inputSignLink.setAttribute("type", "url");
    inputSignLink.setAttribute("name", "signUrl");

    var inputRhyme = document.createElement("textarea");
    inputRhyme.setAttribute("rows", "5");
    inputRhyme.setAttribute("cols", "35");
    inputRhyme.setAttribute("name", "rhyme");
    inputRhyme.setAttribute("style", "vertical-align: middle");

    var inputSong = document.createElement("textarea");
    inputSong.setAttribute("rows", "5");
    inputSong.setAttribute("cols", "35");
    inputSong.setAttribute("name", "song");
    inputSong.setAttribute("style", "vertical-align: middle");

    var inputSongSound = document.createElement("input");
    inputSongSound.setAttribute("type", "file");
    inputSongSound.setAttribute("name", "songSound");

    var inputSongVideo = document.createElement("input");
    inputSongVideo.setAttribute("type", "file");
    inputSongVideo.setAttribute("name", "songVideo");

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupAddBtn");
    inputAddButton.setAttribute("value", getTranslation('Create'));
    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));
    //creating labels for inputs
    
    var labelName = document.createElement("label");
    labelName.htmlFor = "name";
    labelName.innerHTML = getTranslation('Name')+": ";
    var labelImg = document.createElement("label");
    labelImg.htmlFor = "img";
    labelImg.innerHTML = getTranslation('Add image')+": ";
    var labelPictogram = document.createElement("label");
    labelPictogram.htmlFor = "pictogram";
    labelPictogram.innerHTML = getTranslation('Add pictogram')+": ";
    var labelVideo = document.createElement("label");
    labelVideo.htmlFor = "video";
    labelVideo.innerHTML = getTranslation('Add video')+": ";
    var labelSound = document.createElement("label");
    labelSound.htmlFor = "sound";
    labelSound.innerHTML = getTranslation('Add sound')+": ";
    var labelSignImg = document.createElement("label");
    labelSignImg.htmlFor = "sign";
    labelSignImg.innerHTML = getTranslation('Add sign image')+": ";
    var labelSignVideo = document.createElement("label");
    labelSignVideo.htmlFor = "signVideo";
    labelSignVideo.innerHTML = getTranslation('Add sign video')+": ";
    var labelSignLink = document.createElement("label");
    labelSignLink.htmlFor = "signUrl";
    labelSignLink.innerHTML = getTranslation('Add sign video link')+": ";

    var labelRhyme = document.createElement("label");
    labelRhyme.htmlFor = "rhyme";
    labelRhyme.innerHTML = getTranslation('Rhyme - text')+": ";

    var labelSong = document.createElement("label");
    labelSong.htmlFor = "song";
    labelSong.innerHTML = getTranslation('Song - text')+": ";

    var labelSongSound = document.createElement("label");
    labelSongSound.htmlFor = "songSound";
    labelSongSound.innerHTML = getTranslation('Add song')+": ";

    var labelSongVideo = document.createElement("label");
    labelSongVideo.htmlFor = "songVideo";
    labelSongVideo.innerHTML = getTranslation('Add song video')+": ";

    //adding elements to popup
    $("#popupContent").append(iframe);
    $("#popupContent").append(form);

    //hidden value
    form.appendChild(inputCategory);

    //forms
    form.appendChild(labelName);
    form.appendChild(inputName);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelImg);
    form.appendChild(inputImg);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelPictogram);
    form.appendChild(inputPictogram);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelSound);
    form.appendChild(inputSound);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelVideo);
    form.appendChild(inputVideo);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelSignImg);
    form.appendChild(inputSignImg);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(inputRadioVideo);
    form.appendChild(labelSignVideo);
    form.appendChild(inputSignVideo);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(inputRadioLink);
    form.appendChild(labelSignLink);
    form.appendChild(inputSignLink);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelRhyme);
    form.appendChild(inputRhyme);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelSong);
    form.appendChild(inputSong);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelSongSound);
    form.appendChild(inputSongSound);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelSongVideo);
    form.appendChild(inputSongVideo);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(inputAddButton);
    form.appendChild(inputCancelButton);

    //reload after word creation
    form.onsubmit=function () {
    setTimeout(function(){disablePopup();niceAlert(getTranslation('Word created'))},0);
    };

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);
}
function loadEditWordPopup() {
    $("#popupContent").empty();
    //get creator od edit word
    var idcreator;
    var wordId;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/words/" + wordNumber + '?' + lang, false);
    xhr.send(null);
    if (xhr.status == 200) {
        var word = JSON.parse(xhr.responseText);
        idcreator = word.id_creator;
        wordId = word.id;
    }
    else {
        console.log(false)
    }
    //get user id and user role
    var userid = document.getElementById("userid").value;
    var userrole = document.getElementById("userrole").value;

    if((userrole == 'admin') || (idcreator == userid)){
        //creating hidden iframe
        var iframe = document.createElement("iframe");
        iframe.setAttribute('name', "hiddenFrame");
        iframe.setAttribute('id', "hiddenFrame");
        iframe.setAttribute('class', "iframe");
        iframe.style.display = "none";
        //creating form
        var form = document.createElement("form");
        form.setAttribute('action', ApiUrl + '/words/edit/'+ wordId+'?lang='+lang);
        form.setAttribute('method', "post");
        form.setAttribute('enctype', "multipart/form-data");
        form.setAttribute('target', "hiddenFrame");
        //creating inputs
        var inputCategory = document.createElement("input");
        inputCategory.setAttribute("type", "text");
        inputCategory.setAttribute("name", "category");
        inputCategory.setAttribute("value", categoryNumber);
        inputCategory.style.display = "none";
        var inputName = document.createElement("input");
        inputName.setAttribute("type", "text");
        inputName.setAttribute("name", "name");
        inputName.setAttribute("id", "name");
        var inputImg = document.createElement("input");
        inputImg.setAttribute("type", "file");
        inputImg.setAttribute("name", "img");
        var inputPictogram = document.createElement("input");
        inputPictogram.setAttribute("type", "file");
        inputPictogram.setAttribute("name", "pictogram");
        var inputVideo = document.createElement("input");
        inputVideo.setAttribute("type", "file");
        inputVideo.setAttribute("name", "video");
        var inputSound = document.createElement("input");
        inputSound.setAttribute("type", "file");
        inputSound.setAttribute("name", "sound");
        var inputSignImg = document.createElement("input");
        inputSignImg.setAttribute("type", "file");
        inputSignImg.setAttribute("name", "sign");

        var inputRadioVideo = document.createElement("input");
        inputRadioVideo.setAttribute("type", "radio");
        inputRadioVideo.setAttribute("name", "signRadio");
        inputRadioVideo.setAttribute("value", "1");
        inputRadioVideo.setAttribute("checked", "checked");
        var inputRadioLink = document.createElement("input");
        inputRadioLink.setAttribute("type", "radio");
        inputRadioLink.setAttribute("name", "signRadio");
        inputRadioLink.setAttribute("value", "0");

        var inputSignVideo = document.createElement("input");
        inputSignVideo.setAttribute("type", "file");
        inputSignVideo.setAttribute("name", "signVideo");
        var inputSignLink = document.createElement("input");
        inputSignLink.setAttribute("type", "url");
        inputSignLink.setAttribute("name", "signUrl");
        inputSignLink.setAttribute("id", "signUrl");

        var inputRhyme = document.createElement("textarea");
        inputRhyme.setAttribute("rows", "5");
        inputRhyme.setAttribute("cols", "35");
        inputRhyme.setAttribute("name", "rhyme");
        inputRhyme.setAttribute("id", "rhyme");
        inputRhyme.setAttribute("style", "vertical-align: middle");

        var inputSong = document.createElement("textarea");
        inputSong.setAttribute("rows", "5");
        inputSong.setAttribute("cols", "35");
        inputSong.setAttribute("name", "song");
        inputSong.setAttribute("id", "song");
        inputSong.setAttribute("style", "vertical-align: middle");

        var inputSongSound = document.createElement("input");
        inputSongSound.setAttribute("type", "file");
        inputSongSound.setAttribute("name", "songSound");

        var inputSongVideo = document.createElement("input");
        inputSongVideo.setAttribute("type", "file");
        inputSongVideo.setAttribute("name", "songVideo");

        var inputAddButton = document.createElement("input");
        inputAddButton.setAttribute("type", "submit");
        inputAddButton.setAttribute("id", "popupAddBtn");
        inputAddButton.setAttribute("value", getTranslation('Create'));
        var inputCancelButton = document.createElement("input");
        inputCancelButton.setAttribute("type", "button");
        inputCancelButton.setAttribute("id", "popupCloseBtn");
        inputCancelButton.setAttribute("value", getTranslation('Cancel'));
        //creating labels for inputs
        var labelName = document.createElement("label");
        labelName.htmlFor = "name";
        labelName.innerHTML = getTranslation('Name')+": ";
        var labelImg = document.createElement("label");
        labelImg.htmlFor = "img";
        labelImg.innerHTML = getTranslation('Add image')+": ";
        var labelPictogram = document.createElement("label");
        labelPictogram.htmlFor = "pictogram";
        labelPictogram.innerHTML = getTranslation('Add pictogram')+": ";
        var labelVideo = document.createElement("label");
        labelVideo.htmlFor = "video";
        labelVideo.innerHTML = getTranslation('Add video')+": ";
        var labelSound = document.createElement("label");
        labelSound.htmlFor = "sound";
        labelSound.innerHTML = getTranslation('Add sound')+": ";
        var labelSignImg = document.createElement("label");
        labelSignImg.htmlFor = "sign";
        labelSignImg.innerHTML = getTranslation('Add sign image')+": ";
        var labelSignVideo = document.createElement("label");
        labelSignVideo.htmlFor = "signVideo";
        labelSignVideo.innerHTML = getTranslation('Add sign video')+": ";
        var labelSignLink = document.createElement("label");
        labelSignLink.htmlFor = "signUrl";
        labelSignLink.innerHTML = getTranslation('Add sign video link')+": ";

        var labelRhyme = document.createElement("label");
        labelRhyme.htmlFor = "rhyme";
        labelRhyme.innerHTML = getTranslation('Rhyme - text')+": ";

        var labelSong = document.createElement("label");
        labelSong.htmlFor = "song";
        labelSong.innerHTML = getTranslation('Song - text')+": ";

        var labelSongSound = document.createElement("label");
        labelSongSound.htmlFor = "songSound";
        labelSongSound.innerHTML = getTranslation('Add song')+": ";

        var labelSongVideo = document.createElement("label");
        labelSongVideo.htmlFor = "songVideo";
        labelSongVideo.innerHTML = getTranslation('Add song video')+": ";

        //adding elements to popup
        $("#popupContent").append(iframe);
        $("#popupContent").append(form);

        //hidden value
        form.appendChild(inputCategory);

        //forms
        form.appendChild(labelName);
        form.appendChild(inputName);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelImg);
        form.appendChild(inputImg);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelPictogram);
        form.appendChild(inputPictogram);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelSound);
        form.appendChild(inputSound);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelVideo);
        form.appendChild(inputVideo);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelSignImg);
        form.appendChild(inputSignImg);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(inputRadioVideo);
        form.appendChild(labelSignVideo);
        form.appendChild(inputSignVideo);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(inputRadioLink);
        form.appendChild(labelSignLink);
        form.appendChild(inputSignLink);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelRhyme);
        form.appendChild(inputRhyme);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelSong);
        form.appendChild(inputSong);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelSongSound);
        form.appendChild(inputSongSound);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(labelSongVideo);
        form.appendChild(inputSongVideo);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(inputAddButton);
        form.appendChild(inputCancelButton);
    } else {
        //creating hidden iframe
        var iframe = document.createElement("iframe");
        iframe.setAttribute('name', "hiddenFrame");
        iframe.setAttribute('id', "hiddenFrame");
        iframe.setAttribute('class', "iframe");
        iframe.style.display = "none";
        //creating form
        var form = document.createElement("form");
        form.setAttribute('action', ApiUrl + '/words/edit/'+ wordId+'?lang='+lang);
        form.setAttribute('method', "post");
        form.setAttribute('enctype', "multipart/form-data");
        form.setAttribute('target', "hiddenFrame");

        var inputVideo = document.createElement("input");
        inputVideo.setAttribute("type", "file");
        inputVideo.setAttribute("name", "video");
        var inputSound = document.createElement("input");
        inputSound.setAttribute("type", "file");
        inputSound.setAttribute("name", "sound");
        var inputSignImg = document.createElement("input");
        inputSignImg.setAttribute("type", "file");
        inputSignImg.setAttribute("name", "sign");

        var inputRadioVideo = document.createElement("input");
        inputRadioVideo.setAttribute("type", "radio");
        inputRadioVideo.setAttribute("name", "signRadio");
        inputRadioVideo.setAttribute("value", "1");
        inputRadioVideo.setAttribute("checked", "checked");

        var inputAddButton = document.createElement("input");
        inputAddButton.setAttribute("type", "submit");
        inputAddButton.setAttribute("id", "popupAddBtn");
        inputAddButton.setAttribute("value", getTranslation('Save'));
        var inputCancelButton = document.createElement("input");
        inputCancelButton.setAttribute("type", "button");
        inputCancelButton.setAttribute("id", "popupCloseBtn");
        inputCancelButton.setAttribute("value", getTranslation('Cancel'));
        //creating labels for input
        var labelVideo = document.createElement("label");
        labelVideo.htmlFor = "video";
        labelVideo.innerHTML = getTranslation('Add video')+": ";

        //adding elements to popup
        $("#popupContent").append(iframe);
        $("#popupContent").append(form);

        //forms
        form.appendChild(labelVideo);
        form.appendChild(inputVideo);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        var mybr = document.createElement('br');
        form.appendChild(mybr);
        form.appendChild(inputAddButton);
        form.appendChild(inputCancelButton);
    }
    //reload after word editation
    form.onsubmit=function () {
      setTimeout(function(){disablePopup();niceAlert(getTranslation('Word updated'))},0);
    };

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);
}
function disablePopup() {
    $("#toPopup").fadeOut("normal");
    $("#backgroundPopup").fadeOut("normal");
}
function initNewWordPopup() {
    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });
}
function initEditWordPopup() {

    var resultname = "";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/words/" + wordNumber + '?' + lang, false);
    xhr.send(null);
    if (xhr.status == 200) {
        var word = JSON.parse(xhr.responseText);
        resultname = word.name;
    }
    else {
        console.log(false)
    }

    var resultrhyme = "";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/rhymes/" + wordNumber + '?' + lang, false);
    xhr.send(null);
    if (xhr.status == 200) {
        var rhyme = JSON.parse(xhr.responseText);
        resultrhyme = rhyme.rhyme;
    }
    else {
        console.log(false)
    }

    var resultsong = "";
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/songs/" + wordNumber + '?' + lang, false);
    xhr.send(null);
    if (xhr.status == 200) {
        var song = JSON.parse(xhr.responseText);
        resultsong = song.lyrics;
    }
    else {
        console.log(false)
    }
    try {
        var elem = document.getElementById("name");
        elem.value = resultname;

        var elem = document.getElementById("rhyme");
        elem.value = resultrhyme;

        var elem = document.getElementById("song");
        elem.value = resultsong;
    } catch (err){
        console.log(err);
    }

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });
}
function initRemoveWordPopup() {

    $("#popupContent").empty();

    var message = document.createElement("p");
    message.innerHTML = getTranslation('RemoveQuestion');
    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupRemoveBtn");
    inputAddButton.setAttribute("value", getTranslation('Remove'));
    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));

    $("#popupContent").append(message);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    $("#popupContent").append(inputAddButton);
    $("#popupContent").append(inputCancelButton);

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });

    $("#popupRemoveBtn").click(function () {
        removeWord();
    });
}

//------------------------------------------------------------------Users & Categories-----------------------------------------------------------------------

function PopupNewLine() {
    //simple function just for adding next inputs on new line
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
}

// ----------------------Users---------------------------------------------------------
//-----------removeUser
function removeUser(id) {

    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', ApiUrl + '/users/' + id);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                if (this.status == 204) {
                    setTimeout(function(){disablePopup();niceAlert(getTranslation('Removed successfully'))},0);
                   var element = document.getElementById("user&" + id);
                    element.parentNode.removeChild(element);

                }
                else {
                   setTimeout(function(){disablePopup();niceAlert(getTranslation('Remove failed'))},0);


                }
            }
        }
    };
    xhr.send(null);
}


var PopupUserRemove = function (id) {
    id = id.split('&')[1];
    $("#popupContent").empty();
    var message = document.createElement("p");
    message.innerHTML = getTranslation('RemoveUserQuestion');
    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupRemoveBtn");
    inputAddButton.setAttribute("value", getTranslation('Remove'));
    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));

    $("#popupContent").append(message);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    $("#popupContent").append(inputAddButton);
    $("#popupContent").append(inputCancelButton);

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });

    $("#popupRemoveBtn").click(function () {
        removeUser(id);
    });


};

//---------------editUser
function editUser(id) {

    var xhr = new XMLHttpRequest();
    xhr.open("PUT", ApiUrl + '/users/' + id);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 201) {
                setTimeout(function(){disablePopup();niceAlert(getTranslation('User changed'))},0);


            }
            else {
                setTimeout(function(){disablePopup();niceAlert(getTranslation('User change failed'))},0);

            }
        }
    };

    /*
     var nick = 'pepa';
     var name = 'pepa';
     var email = 'email';
     var role = 0;
     var valid = 1;
     */

    /*var nick = document.getElementById('PopUserNick').value;
    var name = document.getElementById('PopUserName').value;
    var email = document.getElementById('PopUserEmail').value;*/
    var role = document.getElementsByName('RoleRadio');
    var valid = document.getElementsByName('ValidRadio');

    if (role[0].checked) {
        role = role[0].value;
    } else {
        role = role[1].value;
    }
    if (valid[0].checked) {
        valid = valid[0].value;
    } else {
        valid = valid[1].value;
    }

    xhr.send(JSON.stringify({/*nick: nick, name: name, email: email,*/ role: role, valid: valid }));
}


function PopupUserEdit(user) {
    //console.log('id=' + user.id);

    id = user.id;

    $("#popupContent").empty();

    /*  var labelName = document.createElement("label");
     labelName.htmlFor = "PopUserName";
     labelName.innerHTML = "Name ";
     $("#popupContent").append(labelName);

     var inputName = document.createElement("input");
     inputName.setAttribute("type", "text");
     inputName.setAttribute("id", "PopUserName");
     inputName.setAttribute("value", user.name);
     $("#popupContent").append(inputName);

     PopupNewLine();

     var labelName = document.createElement("label");
     labelName.htmlFor = "PopUserName";
     labelName.innerHTML = "Nick ";
     $("#popupContent").append(labelName);

     var inputName = document.createElement("input");
     inputName.setAttribute("type", "text");
     inputName.setAttribute("id", "PopUserNick");
     inputName.setAttribute("value", user.nick);
     $("#popupContent").append(inputName);

     PopupNewLine();

     var labelName = document.createElement("label");
     labelName.htmlFor = "PopUserName";
     labelName.innerHTML = "Email";
     $("#popupContent").append(labelName);

     var inputName = document.createElement("input");
     inputName.setAttribute("type", "text");
     inputName.setAttribute("id", "PopUserEmail");
     inputName.setAttribute("value", user.email);
     $("#popupContent").append(inputName);

     PopupNewLine();*/

    var labelName = document.createElement("label");
    labelName.innerHTML = "admin";
    $("#popupContent").append(labelName);

    var inputRadioRole = document.createElement("input");
    inputRadioRole.setAttribute("type", "radio");
    inputRadioRole.setAttribute("name", "RoleRadio");
    inputRadioRole.setAttribute("value", "0");
    $("#popupContent").append(inputRadioRole);

    var labelName = document.createElement("label");
    labelName.innerHTML = "user";
    $("#popupContent").append(labelName);

    var inputRadioRole2 = document.createElement("input");
    inputRadioRole2.setAttribute("type", "radio");
    inputRadioRole2.setAttribute("name", "RoleRadio");
    inputRadioRole2.setAttribute("value", "1");
    $("#popupContent").append(inputRadioRole2);

    user.role == 0 ? inputRadioRole.setAttribute("checked", true) : inputRadioRole2.setAttribute("checked", true);
    PopupNewLine();

    var labelName = document.createElement("label");
    labelName.innerHTML = "valid";
    $("#popupContent").append(labelName);
    //console.log(user.valid == 1 ? true : false);
    var inputRadioRole = document.createElement("input");
    inputRadioRole.setAttribute("type", "radio");
    inputRadioRole.setAttribute("name", "ValidRadio");
    inputRadioRole.setAttribute("value", "1");
    inputRadioRole.setAttribute("checked", true);
    $("#popupContent").append(inputRadioRole);

    var labelName = document.createElement("label");
    labelName.innerHTML = "invalid";
    $("#popupContent").append(labelName);

    var inputRadioRole2 = document.createElement("input");
    inputRadioRole2.setAttribute("type", "radio");
    inputRadioRole2.setAttribute("name", "ValidRadio");
    inputRadioRole2.setAttribute("value", "0");
    $("#popupContent").append(inputRadioRole2);

    user.valid == 1 ? inputRadioRole.setAttribute("checked", true) : inputRadioRole2.setAttribute("checked", true);

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupAddBtn");
    inputAddButton.setAttribute("value", getTranslation('Save'));

    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));


    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    $("#popupContent").append(inputAddButton);
    $("#popupContent").append(inputCancelButton);

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });

    $("#popupAddBtn").click(function () {
        editUser(id);
    });

}

function UserEdit(id) {
    id = id.split('&')[1];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + '/users/');
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                var users = JSON.parse(this.responseText);
                for (index = 0; index < users.length; index++) {
                    if (users[index].id == id) {
                        PopupUserEdit(users[index]);

                    }
                }
            }
        }
    };
    xhr.send(null);
}


//Vojta----------------------Categories---------------------------------------------------------
//-------------RemoveCat

function removeCat(id) {
    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', ApiUrl + '/categories/' + id);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                if (this.status == 204) {
                    setTimeout(function(){disablePopup();niceAlert(getTranslation('Removed successfully'))},0);
                    var element = document.getElementById("cat&" + id);
                    element.parentNode.removeChild(element);
                    var element = document.getElementById("catWords&" + id);
                    element.parentNode.removeChild(element);
                }
                else {
                    setTimeout(function(){disablePopup();niceAlert(getTranslation('Remove failed'))},0);
                 }
            }
        }
    };
    xhr.send(null);
}


var PopupCatRemove = function (id) {
    id = id.split('&')[1];
    $("#popupContent").empty();
    var message = document.createElement("p");
    message.innerHTML = getTranslation('RemoveCategoryQuestion');
    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupRemoveBtn");
    inputAddButton.setAttribute("value", getTranslation('Remove'));
    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));

    $("#popupContent").append(message);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    var mybr = document.createElement('br');
    $("#popupContent").append(mybr);
    $("#popupContent").append(inputAddButton);
    $("#popupContent").append(inputCancelButton);

    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });

    $("#popupRemoveBtn").click(function () {
        removeCat(id);
    });


};

function addNewCatPopup(type, cat) {
    $("#popupContent").empty();

    //creating hidden iframe
    var iframe = document.createElement("iframe");
    iframe.setAttribute('name', "hiddenFrame");
    iframe.setAttribute('id', "hiddenFrame");
    iframe.setAttribute('class', "iframe");
    iframe.style.display = "none";
    //creating form
    var form = document.createElement("form");
    if (type == 'edit') {form.setAttribute('action', ApiUrl+'/categories/edit/'+ cat.id+'?lang='+lang);}
    else{form.setAttribute('action', ApiUrl+'/categories'+'?lang='+lang);}
    form.setAttribute('method', "post");
    form.setAttribute('enctype', "multipart/form-data");
    form.setAttribute('target', "hiddenFrame");
    //inputs
    var inputName = document.createElement("input");
    inputName.setAttribute("type", "text");
    inputName.setAttribute("name", "name");
    inputName.setAttribute("id", "name");
    if (type == 'edit') {inputName.setAttribute("value", cat.name);}

    var inputImg = document.createElement("input");
    inputImg.setAttribute("type", "file");
    inputImg.setAttribute("name", "img");
    inputImg.setAttribute("id", "img");

    var inputAddButton = document.createElement("input");
    inputAddButton.setAttribute("type", "submit");
    inputAddButton.setAttribute("id", "popupAddBtn");
    if (type == 'edit') {inputAddButton.setAttribute("value", getTranslation('Save'));}
    else{inputAddButton.setAttribute("value", getTranslation('Create'));}

    var inputCancelButton = document.createElement("input");
    inputCancelButton.setAttribute("type", "button");
    inputCancelButton.setAttribute("id", "popupCloseBtn");
    inputCancelButton.setAttribute("value", getTranslation('Cancel'));

    //labels
    var labelName = document.createElement("label");
    labelName.htmlFor = "name";
    labelName.innerHTML = getTranslation('Name')+": ";

    var labelImg = document.createElement("label");
    labelImg.htmlFor = "img";
    labelImg.innerHTML = getTranslation('Add image')+": ";

    //adding elements to popup
    $("#popupContent").append(iframe);
    $("#popupContent").append(form);
    //forms
    form.appendChild(labelName);
    form.appendChild(inputName);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(labelImg);
    form.appendChild(inputImg);

    var mybr = document.createElement('br');
    form.appendChild(mybr);
    var mybr = document.createElement('br');
    form.appendChild(mybr);
    form.appendChild(inputAddButton);
    form.appendChild(inputCancelButton);

    //reload after word creation
    form.onsubmit=function () {

           if (type == 'edit') {setTimeout(function(){disablePopup();niceAlert(getTranslation('Category changed'))},0);}
           else {
           setTimeout(function(){disablePopup();niceAlert(getTranslation('Category created'))},0);

           };


       };
    //popup appearance
    $("#toPopup").fadeIn(500);
    $("#backgroundPopup").css("opacity", "0.7");
    $("#backgroundPopup").fadeIn(1);

    $(this).keyup(function (event) {
        if (event.which == 27) {
            disablePopup();
        }
    });

    $("#backgroundPopup").click(function () {
        disablePopup();
    });

    $("#popupCloseBtn").click(function () {
        disablePopup();
    });

}

function addNewCat(type, id) {
    if (type == 'add') {
        addNewCatPopup(type, null);

    }
    else {
        id = id.split('&')[1];
        var xhr = new XMLHttpRequest();
        xhr.open('GET', ApiUrl + '/categories/' + id + '?lang=' + lang);
        xhr.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (typeof cb !== "undefined") {
                    cb(this);
                }
                else {
                    var cat = JSON.parse(this.responseText);
                    addNewCatPopup('edit', cat);

                }
            }
        }

        xhr.send(null);

    }


}